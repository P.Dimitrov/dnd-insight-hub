﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using InsightHub.Blob.Service.Contracts;
using InsightHub.Blob.Service.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Storage;
using Microsoft.Azure.Storage.Auth;
using Microsoft.Azure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;

namespace InsightHub.Blob.Service
{
    /// <summary>
    /// A class that implements functionalities needed to maintain files in Azure Blob Storage - upload, download and delete.
    /// </summary>
    public class BlobService : IBlobService
    {
        private readonly BlobServiceClient blobServiceClient;

        public BlobService(BlobServiceClient blobServiceClient)
        {
            this.blobServiceClient = blobServiceClient;
        }

        public async Task<MemoryStream> DownloadFileAsync(string name, string oldName)
        {

            var storageAccount = new CloudStorageAccount(
                new StorageCredentials("insighthubfileaccount", "Ashx0ZbwWLLwIOkfJr5xsjKAhsf7qXW43DH0KGY+Es094oh4kuk/e71jkezcBBZ1ao456Sjl/m+1wXJto6oA7g=="), true);

            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference($"reportsfilecontainer/{name}");

            var blob = container.GetBlockBlobReference($"{oldName}");

            var memory = new MemoryStream();

            await blob.DownloadToStreamAsync(memory);

            memory.Position = 0;

            return memory;
        }

        public async Task UploadFileAsync(IFormFile file, string newFolder)
        {
            const string accountName = "insighthubfileaccount";
            const string key = "Ashx0ZbwWLLwIOkfJr5xsjKAhsf7qXW43DH0KGY+Es094oh4kuk/e71jkezcBBZ1ao456Sjl/m+1wXJto6oA7g==";

            var storageAccount = new CloudStorageAccount(new StorageCredentials(accountName, key), true);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("reportsfilecontainer");
            await container.CreateIfNotExistsAsync();
            await container.SetPermissionsAsync(new BlobContainerPermissions()
            {
                PublicAccess = BlobContainerPublicAccessType.Blob
            });

            CloudBlobDirectory folder = container.GetDirectoryReference(newFolder);

            var blockblob = folder.GetBlockBlobReference(file.FileName);

            using (var stream = file.OpenReadStream())
            {
                await blockblob.UploadFromStreamAsync(stream);
            }
        }

        public async Task DeleteFileAsync(string folder, string fileTitle)
        {
            var containerClinet = blobServiceClient.GetBlobContainerClient($"reportsfilecontainer/{folder}");
            var blobClient = containerClinet.GetBlobClient(fileTitle);
            await blobClient.DeleteIfExistsAsync();
        }

    }
}
