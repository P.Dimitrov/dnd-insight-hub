﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data;
using InsightHub.Models;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Services.Contracts;
using InsightHub.Web.Models;
using InsightHub.Web.Areas.Client.Models;
using InsightHub.Services.DTO;

namespace InsightHub.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class IndustriesController : Controller
    {
        private readonly IIndustryService industryService;

        public IndustriesController(IIndustryService industryService)
        {
            this.industryService = industryService;
        }

        // GET: Admin/Industries
        public async Task<IActionResult> Index()
        {
            var industries = await this.industryService.GetAllIndustriesAsync();

            var industryViewModels = industries.Select(i => new IndustryIndexViewModel(i));

            return View(industryViewModels);
        }

        // GET: Admin/Industries/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Industries/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] IndustryViewModel industry)
        {
            if (ModelState.IsValid)
            {
                var industryDto = new IndustryDTO
                {
                    Name = industry.Name
                };

                await this.industryService.CreateIndustryAsync(industryDto);
            }
            return RedirectToAction(nameof(Index), "Industries");
        }

        // GET: Admin/Industries/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var industry = await this.industryService.GetIndustryAsync(id);

            if (industry == null)
            {
                return NotFound();
            }

            var industryViewModel = new IndustryIndexViewModel(industry);

            return View(industryViewModel);
        }

        // POST: Admin/Industries/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [FromForm] IndustryIndexViewModel industry)
        {
            if (id != industry.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var industryDto = new IndustryDTO
                {
                    Id = id,
                    Name = industry.Name
                };

                await this.industryService.UpdateIndustryAsync(industryDto);

                return RedirectToAction(nameof(Index), "Industries");
            }
            return RedirectToAction(nameof(Index), "Industries");
        }

        // GET: Admin/Industries/Delete/5
        public async Task<IActionResult> Delete(int id)
        {

            var industry = await this.industryService.GetIndustryAsync(id);

            if (industry == null)
            {
                return NotFound();
            }

            var industryViewModel = new IndustryIndexViewModel(industry);

            return View(industryViewModel);
        }

        // POST: Admin/Industries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await this.industryService.DeleteIndustryAsync(id);

            return RedirectToAction(nameof(Index), "Industries");
        }
    }
}
