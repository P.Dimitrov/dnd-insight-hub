﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Models;
using Microsoft.AspNetCore.Authorization;

namespace InsightHub.Web.Areas.Admin.Controllers
{
    /// <summary>
    /// Provides functionality to the /User/ route in the Admin area:
    ///   -  Displays all approved users index page
    ///    - Displays all pending users index page
    ///     - Displays action to approve a user
    ///      - Displays action to ban a user
    ///        - Displays action to get a user by ID
    ///         - Displays action to delete a user
    /// </summary>
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        public async Task<IActionResult> Index()
        {
            if (User.IsInRole("Admin"))
            {
                var users = await this.userService.GetAllApprovedUsersAsync();
                var userViewModels = users.Select(u => new UserIndexViewModel(u));

                return View(userViewModels);
            }

            return RedirectToAction(nameof(Index), "Home");
        }

        // GET: Pending Users
        public async Task<IActionResult> Pending()
        {
            if (User.IsInRole("Admin"))
            {
                var users = await this.userService.GetAllPendingUsersAsync();
                var userViewModels = users.Select(u => new UserIndexViewModel(u));

                return View(userViewModels);
            }

            return RedirectToAction(nameof(Index), "Home");
        }

        // Post: Approve User
        public async Task<IActionResult> Approve(int id)
        {
            if (User.IsInRole("Admin"))
            {
                var user = await this.userService.ApproveUserAsync(id);

                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index), "Home");
        }

        // Post: Ban User
        public async Task<IActionResult> Ban(int Id)
        {
            if (User.IsInRole("Admin"))
            {
                var user = await this.userService.BanUserAsync(Id);
                var userViewModel = new UserIndexViewModel(user);

                return RedirectToAction(nameof(Pending));
            }

            return RedirectToAction(nameof(Index), "Home");
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (User.IsInRole("Admin"))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var user = await this.userService.GetUserAsync(id);

                if (user == null)
                {
                    return NotFound();
                }

                var userViewModel = new UserViewModel(user);

                return View(userViewModel);
            }

            return RedirectToAction(nameof(Index), "Home");
            
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (User.IsInRole("Admin"))
            {
                if (id == null)
                {
                    return NotFound();
                }

                var user = await this.userService.GetUserAsync(id);

                if (user == null)
                {
                    return NotFound();
                }

                var userViewModel = new UserViewModel(user);

                return View(userViewModel);
            }

            return RedirectToAction(nameof(Index), "Home");
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (User.IsInRole("Admin"))
            {
                var user = await this.userService.DeleteUserAsync(id);

                return RedirectToAction(nameof(Index));
            }

            return RedirectToAction(nameof(Index), "Home");
        }
    }
}
