﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Web.Areas.Admin.Models;
using InsightHub.Web.Models;
using InsightHub.Services.DTO;

namespace InsightHub.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TagsController : Controller
    {
        private readonly ITagService tagService;

        public TagsController(ITagService tagService)
        {
            this.tagService = tagService;
        }

        // GET: Admin/Tags
        public async Task<IActionResult> Index()
        {
            var tags = await this.tagService.GetAllTagsAsync();

            var tagViewModels = tags.Select(i => new TagIndexViewModel(i));

            return View(tagViewModels);
        }

        // GET: Admin/Tags/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/Tags/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] TagIndexViewModel tag)
        {
            if (ModelState.IsValid)
            {
                var tagDTO = new TagDTO
                {
                    Name = tag.Name
                };

                await this.tagService.CreateTagAsync(tagDTO);

                return RedirectToAction(nameof(Index), "Tags");
            }
            return View(tag);
        }

        // GET: Admin/Tags/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var tag = await tagService.GetTagAsync(id);

            if (tag == null)
            {
                return NotFound();
            }

            var tagViewModel = new TagIndexViewModel
            {
                Name = tag.Name
            };

            return View(tagViewModel);
        }

        // POST: Admin/Tags/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditConfirmed(int id, [FromForm] TagIndexViewModel tag)
        {
            if (id != tag.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var tagDTO = new TagDTO
                {
                    Name = tag.Name
                };

                await this.tagService.UpdateTagAsync(id, tagDTO);
            }
            return RedirectToAction(nameof(Index), "Tags");
        }

        // GET: Admin/Tags/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (id == default)
            {
                return NotFound();
            }

            var tag = await this.tagService.GetTagAsync(id);

            var tagViewModel = new TagIndexViewModel
            {
                Id = tag.Id,
                Name = tag.Name
            };
            return View(tagViewModel);
        }

        // POST: Admin/Tags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tag = await this.tagService.DeleteTagAsync(id);

            return RedirectToAction(nameof(Index), "Tags");
        }
    }
}
