﻿using InsightHub.Models;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Admin.Models
{
    /// <summary>
    /// A class that maps ReportDTO into Report View Model for Admin area.
    /// </summary>
    public class ReportAdminViewModel
    {
        public ReportAdminViewModel(ReportDTO reportDTO)
        {

            Id = reportDTO.Id;
            Name = reportDTO.Name;
            Description = reportDTO.Description;
            Industry = reportDTO.Industry.Name;
            Author = reportDTO.Author;
            FileName = reportDTO.FileName;
            TagsId = reportDTO.Tags.Select(tagId => tagId.Id).ToList();
            File = reportDTO.File;
            IsApproved = reportDTO.IsApproved;
            Tags = reportDTO.Tags.Select(t => t.Name).ToList();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string Industry { get; set; }

        public string Author { get; set; }

        public ICollection<int> TagsId { get; set; }

        public ICollection<string> Tags { get; set; }

        public string FileName { get; set; }

        public IFormFile File { get; set; }

        public bool IsApproved { get; set; }

        public bool IsFeatured { get; set; }
    }
}
