﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Admin.Models
{
    /// <summary>
    /// A class that maps ReportDTO into Report Index View Model for Admin area.
    /// </summary>
    public class ReportIndexViewModel
    {
        public ReportIndexViewModel(ReportDTO report)
        {
            Id = report.Id;
            Name = report.Name;
            Author = report.Author;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
    }
}
