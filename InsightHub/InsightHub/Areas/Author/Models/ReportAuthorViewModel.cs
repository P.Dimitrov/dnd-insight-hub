﻿using InsightHub.Services.DTO;
using InsightHub.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Author.Models
{
    /// <summary>
    /// A class that maps ReportDTO into Report View Model for Author area.
    /// </summary>
    public class ReportAuthorViewModel
    {
        public ReportAuthorViewModel(ReportDTO report)
        {
            Id = report.Id;
            Name = report.Name;
            CreatedOn = report.CreatedOn;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
