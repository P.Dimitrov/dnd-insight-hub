﻿using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTO;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Web.Areas.Author.Models;
using InsightHub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Areas.Author.Controllers
{
    /// <summary>
    /// Provides functionality to the /Report/ route in the Author area:
    ///   -  Displays all authored reports index page
    ///     - Displays action to create a report
    ///      - Displays action to edit a report
    ///         - Displays action to delete a report
    /// </summary>
    [Area("Author")]
    [Authorize(Roles = "Author")]
    public class ReportsController : Controller
    {
        private readonly InsightHubContext context;
        private readonly IReportService reportService;
        private readonly ITagService tagService;
        private readonly IIndustryService industryService;
        private readonly IUserService userService;
        private readonly IFileTypeProvider fileTypeProvider;

        public ReportsController(InsightHubContext context, IReportService reportService,
            ITagService tagService, IIndustryService industryService, IUserService userService, IFileTypeProvider fileTypeProvider)
        {
            this.context = context;
            this.reportService = reportService;
            this.tagService = tagService;
            this.industryService = industryService;
            this.userService = userService;
            this.fileTypeProvider = fileTypeProvider;
        }

        // GET: Author/Reports
        public async Task<IActionResult> ShowMyReports()
        {
            var authoredReports = await this.reportService.GetAllAuthoredReportsAsync(User.Identity.Name);
            var reportsViewModels = authoredReports.Select(r => new ReportAuthorViewModel(r));

            return View(reportsViewModels);
        }

        // GET: Reports/Create
        public async Task<IActionResult> Create()
        {
            ViewData["Industry"] = new SelectList(await industryService.GetAllIndustriesAsync(), "Name", "Name", "Name");
            ViewData["TagName"] = new SelectList(await tagService.GetAllTagsAsync(), "Name", "Name");

            return View();
        }

        // POST: Reports/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromForm] CreateReportViewModel report)
        {
            if (ModelState.IsValid)
            {
                var author = await this.userService.GetUserByNameAsync(User.Identity.Name);

                if (report.File.Length > 25*1024*1024)
                {
                    return RedirectToAction(nameof(Create), "Reports");
                }

                var reportDTO = new ReportDTO
                {
                    Name = report.Name,
                    Description = report.Description,
                    IndustryName = report.IndustryName,
                    AuthorID = author.Id,
                    Author = author.UserName,
                    Tags = report.TagName.Select(tag => new TagDTO { Name = tag }).ToList(),
                    File = report.File,
                    FileName = report.File.FileName
                };

                await this.reportService.CreateReportAsync(reportDTO);

                return RedirectToAction(nameof(ShowMyReports), "Reports");

            }

            ViewData["Industry"] = new SelectList(await industryService.GetAllIndustriesAsync(), "Id", "Name", report.Industry);
            ViewData["TagsId"] = new SelectList(await tagService.GetAllTagsAsync(), "Id", "Name", report.TagName);

            return View(report);
        }

        // GET: Reports/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var report = await reportService.GetReportAsync(id);

            var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

            if (report.AuthorID != userID)
            {
                return BadRequest();
            }

            var reportViewModel = new AuthorReportViewModel
            {
                Id = report.Id,
                Name = report.Name,
                Description = report.Description,
                AuthorID = report.AuthorID,
                Author = report.Author,
                IndustryID = report.Industry.Id,
                Industry = report.Industry,
                TagName = report.Tags.Select(x => x.Name).ToList(),
                FileName = report.FileName
            };

            ViewData["IndustryName"] = new SelectList(await industryService.GetAllIndustriesAsync(), "Name", "Name");
            ViewData["TagName"] = new SelectList(await tagService.GetAllTagsAsync(), "Name", "Name");

            return View(reportViewModel);
        }

        // POST: Reports/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditConfirmed(int id, [FromForm] AuthorReportViewModel report)
        {
            if (ModelState.IsValid)
            {
                var author = await this.userService.GetUserByNameAsync(User.Identity.Name);

                var reportDTO = new ReportDTO
                {
                    Name = report.Name,
                    Description = report.Description,
                    IndustryName = report.IndustryName,
                    AuthorID = author.Id,
                    Author = author.UserName,
                    Tags = report.TagName.Select(tag => new TagDTO { Name = tag }).ToList(),
                    File = report.File,
                    FileName = report.FileName
                };

                await this.reportService.UpdateReportAsync(id, reportDTO);
            }

            ViewData["IndustryName"] = new SelectList(await industryService.GetAllIndustriesAsync(), "Name", "Name", report.Industry);
            ViewData["TagName"] = new SelectList(await tagService.GetAllTagsAsync(), "Name", "Name", report.TagName);

            return RedirectToAction(nameof(ShowMyReports), "Reports");
        }

        // GET: Reports/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            if (id == default)
            {
                return NotFound();
            }

            var report = await this.reportService.GetReportAsync(id);

            var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);

            if (report.AuthorID != userID && !User.IsInRole("Admin"))
            {
                return BadRequest();
            }

            if (report == null)
            {
                return NotFound();
            }

            var reportViewModel = new AuthorReportViewModel
            {
                Id = report.Id,
                Name = report.Name,
                Description = report.Description,
                Industry = report.Industry
            };
            return View(reportViewModel);
        }

        // POST: Reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var report = await this.reportService.DeleteReportAsync(id);

            return RedirectToAction(nameof(ShowMyReports));
        }
    }
}
