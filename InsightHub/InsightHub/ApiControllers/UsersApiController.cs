﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using InsightHub.Models;
using InsightHub.Web.ApiControllers.Helpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.IdentityModel.Tokens;

namespace InsightHub.Web.ApiControllers
{
    [Route("api/login")]
    [ApiController]
    public class UsersApiController : Controller
    {
        private readonly SignInManager<User> signInManager;
        private readonly UserManager<User> userManager;

        public UsersApiController(SignInManager<User> signInManager, UserManager<User> userManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
        }
        [Route("")]
        [HttpPost]
        public async Task<IActionResult> CreateToken([FromBody] JwtLoginModel user)
        {
            if (ModelState.IsValid)
            {
                User appUser = await userManager.FindByNameAsync(user.UserName);
                var signInResult = await signInManager.CheckPasswordSignInAsync(appUser, user.Password, false);
                if (signInResult.Succeeded)
                {
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(InsightHubJWTTokens.Key));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var claims = new[]
                    {
                        new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
                    };

                    var token = new JwtSecurityToken(InsightHubJWTTokens.Issuer, InsightHubJWTTokens.Audience,
                        claims, 
                        expires: DateTime.UtcNow.AddMinutes(30), 
                        signingCredentials: creds);

                    var result = new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo
                    };

                    return Created("", result);
                }
                else
                {
                    return BadRequest();
                }
            }
            return BadRequest();
        }
    }
}