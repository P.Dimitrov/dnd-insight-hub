﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Authorization;
using InsightHub.Web.ApiControllers.Helpers;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    /// API Controller that:
    ///  - provides information for a list of all industries
    ///   - provides information for a single industry by Id
    ///    - creates a new industry
    ///     - updates a current industry
    ///      - deletes an industry
    /// </summary>
    [Route("api/industries")]
    [ApiController]
    [Authorize(AuthenticationSchemes = InsightHubJWTTokens.AuthSchemes)]
    public class IndustriesApiController : ControllerBase
    {
        private readonly IIndustryService industryService;

        public IndustriesApiController(IIndustryService industryService)
        {
            this.industryService = industryService;
        }

        // GET: api/Industries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<IndustryDTO>>> GetIndustries()
        {
            try
            {
                var industries = await this.industryService.GetAllIndustriesAsync();

                return Ok(industries);
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        // GET: api/Industries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<IndustryDTO>> GetIndustry(int id)
        {
            try
            {
                var industry = await this.industryService.GetIndustryAsync(id);

                return Ok(industry);
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        // PUT: api/Industries/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutIndustry(int id, IndustryDTO industry)
        {
            try
            {
                industry.Id = id;

                var industryDtoToReturn = await this.industryService.UpdateIndustryAsync(industry);

                if (industryDtoToReturn == null)
                {
                    return NotFound();
                }

                return Ok(industryDtoToReturn);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // POST: api/Industries
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<IndustryDTO>> PostIndustry(IndustryDTO industry)
        {
            try
            {
                var industryDto = await this.industryService.CreateIndustryAsync(industry);

                return CreatedAtAction("GetIndustry", new { id = industryDto.Id });
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: api/Industries/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<IndustryDTO>> DeleteIndustry(int id)
        {
            var industry = await this.industryService.GetIndustryAsync(id);

            if (industry == null)
            {
                return NotFound();
            }

            var industryDto = new IndustryDTO
            {
                Name = industry.Name
            };

            await this.industryService.DeleteIndustryAsync(id);

            return industryDto;
        }
    }
}
