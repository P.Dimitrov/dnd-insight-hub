﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InsightHub.Services.Contracts;
using InsightHub.Services.DTO;
using InsightHub.Web.ApiControllers.Helpers;
using InsightHub.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace InsightHub.Web.ApiControllers
{
    /// <summary>
    /// API Controller that:
    ///  - provides information for a list of all reports
    ///   - provides information for a single report by Id
    ///    - provides information for five featured reports
    ///     - provides information for five most downloaded reports
    ///      - provides information for five newest reports
    ///       - creates a new report
    ///        - updates a current report
    ///         - deletes a report
    /// </summary>
    [Route("api/reports")]
    [ApiController]
    [Authorize(AuthenticationSchemes = InsightHubJWTTokens.AuthSchemes)]
    public class ReportsApiController : ControllerBase
    {
        private readonly IReportService reportService;

        public ReportsApiController(IReportService reportService)
        {
            this.reportService = reportService;
        }

        // GET: api/ReportsApi
        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAllReports()
        {
            try
            {
                var reports = await this.reportService.GetAllReportsAsync();
                return Ok(reports);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("report/recent")]
        public async Task<IActionResult> GetFiveNewestReports()
        {
            try
            {
                var reports = await this.reportService.GetFiveNewestReportsAsync();
                return Ok(reports);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("report/featured")]
        public async Task<IActionResult> GetFeaturedReports()
        {
            try
            {
                var reports = await this.reportService.GetFeaturedReportsAsync();
                return Ok(reports);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("report/mostdownloaded")]
        public async Task<IActionResult> GetFiveMostDownloadedReports()
        {
            try
            {
                var reports = await this.reportService.GetFiveMostDownloadedReportsAsync();
                return Ok(reports);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // GET: api/ReportsApi/5
        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetReport(int id)
        {
            try
            {
                var report = await this.reportService.GetReportAsync(id);

                return Ok(report);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // PUT: api/ReportsApi/5
        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> PutReport(int id, [FromForm]InputReportModel report)
        {
            try
            {
                var reportDTO = new ReportDTO
                {
                    Name = report.Name,
                    Description = report.Description,
                    AuthorID = report.AuthorId,
                    IndustryID = report.IndustryId,
                    Tags = report.TagsId.Select(tag => new TagDTO { Id = tag }).ToList(),
                    FileName = report.File.FileName,
                    File = report.File
                };

                var updatedReportDTO = await this.reportService.UpdateReportAsync(id, reportDTO);

                return Ok(updatedReportDTO);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        // POST: api/ReportsApi
        [HttpPost]
        [Route("")]
        public async Task<IActionResult> PostReport([FromForm]InputReportModel report)
        {
            try
            {
                var reportDTO = new ReportDTO
                {
                    Name = report.Name,
                    Description = report.Description,
                    AuthorID = report.AuthorId,
                    IndustryID = report.IndustryId,
                    Tags = report.TagsId.Select(tag => new TagDTO { Id = tag }).ToList(),
                    FileName = report.File.FileName,
                    File = report.File
                };

                if (reportDTO == null)
                {
                    return BadRequest();
                }
                var newReportDTO = await this.reportService.CreateReportAsync(reportDTO);
                return CreatedAtAction("GetReport", newReportDTO);
            }

            catch (Exception)
            {
                return BadRequest();
            }
        }

        //[HttpPost]
        //[Route("{download}/{id}")]
        //public async Task<IActionResult> DownloadReport(int id, [FromBody] ReportViewModel file)
        //{
        //    try
        //    {
        //        bool isDownloaded = await this.reportService.DownloadReportAsync(id, file.FilePath);

        //        return Ok($"Successfully downloaded report with ID: {file.Id}!");
        //    }
        //    catch (Exception)
        //    {
        //        return BadRequest($"Report with ID: {file.Id} was not found!");
        //    }
        //}

        // DELETE: api/ReportsApi/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReport(int id)
        {
            try
            {
                var reports = await this.reportService.DeleteReportAsync(id);

                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}