﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.ApiControllers.Helpers
{
    public class JwtLoginModel
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
