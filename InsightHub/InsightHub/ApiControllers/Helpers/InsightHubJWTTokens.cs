﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Web.ApiControllers.Helpers
{
    public class InsightHubJWTTokens
    {
        public const string Issuer = "InsightHub";

        public const string Audience = "ApiUser";

        public const string Key = "1ns1gh74u5d4ndd1";

        public const string AuthSchemes = "Identity.Application" + "," + JwtBearerDefaults.AuthenticationScheme;
    }
}
