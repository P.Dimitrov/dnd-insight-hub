﻿using InsightHub.Data;
using InsightHub.Services.Contracts;
using InsightHub.Services.Providers.Contracts;
using InsightHub.Web.Models;
using InsightHub.Web.Models.ReportMVCModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Controllers
{
    /// <summary>
    /// Provides functionality to the /Report/ route:
    ///   -  Displays a reports' index page
    ///    - Displays a reports' details page
    ///     - Displays a reports' download action
    /// </summary>
    public class ReportsController : Controller
    {
        private readonly InsightHubContext context;
        private readonly IReportService reportService;
        private readonly ITagService tagService;
        private readonly IIndustryService industryService;
        private readonly IUserService userService;
        private readonly IFileTypeProvider fileTypeProvider;

        public ReportsController(InsightHubContext context, IReportService reportService, 
            ITagService tagService, IIndustryService industryService, IUserService userService, IFileTypeProvider fileTypeProvider)
        {
            this.context = context;
            this.reportService = reportService;
            this.tagService = tagService;
            this.industryService = industryService;
            this.userService = userService;
            this.fileTypeProvider = fileTypeProvider;
        }

        // GET: Reports
        public async Task<IActionResult> Index()
        {
            var reports = await this.reportService.GetAllApprovedReportsAsync();
            
            var reportsViewModel = reports.Select(r => new IndexReportViewModel
            {
                Id = r.Id,
                Name = r.Name,
                Description = r.Description,
                Industry = new IndustryViewModel { Id = r.Industry.Id, Name = r.Industry.Name},
                Author = r.Author,
                Tags = r.Tags.Select(tag => new TagViewModel { Id = tag.Id, Name = tag.Name }).ToList(),
                CreatedOn = r.CreatedOn,
                TimesDownloaded = r.TimesDownloaded
            });

            return View(reportsViewModel);
        }

        // GET: Reports/Details/5
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var report = await this.reportService.GetReportAsync(id);

                var reportsViewModel = new DetailsMVCViewModel
                {
                    Id = report.Id,
                    Name = report.Name,
                    Description = report.Description,
                    Industry = new IndustryViewModel { Id = report.Industry.Id, Name = report.Industry.Name },
                    Author = report.Author,
                    Tags = report.Tags.Select(tag => new TagViewModel { Id = tag.Id, Name = tag.Name }).ToList(),
                    TimesDownloaded = report.TimesDownloaded,
                    FileName = report.FileName
                };

                if (report == null)
                {
                    return NotFound();
                }

                return View(reportsViewModel);
            }
            catch (Exception)
            {
                return NotFound();
            }

        }


        public async Task<IActionResult> DownloadFile(int id)
        {
            try
            {
                var userID = int.Parse(User.FindFirst(System.Security.Claims.ClaimTypes.NameIdentifier).Value);
                var report = await this.reportService.GetReportAsync(id);
                if (report == null)
                {
                    return BadRequest();
                }
                var memory = await this.reportService.DownloadReportAsync(id, userID);

                var ext = fileTypeProvider.GetFileType(report.FileName);

                return File(memory, ext, report.FileName);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        
    }
}
