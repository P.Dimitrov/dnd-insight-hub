﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Web.Models;

namespace InsightHub.Controllers
{
    /// <summary>
    /// Provides functionality to the /Home/ route:
    ///  -  Displays a home page
    ///   -  Displays a reports index page.
    /// </summary>
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IReportService reportService;

        public HomeController(ILogger<HomeController> logger, IReportService reportService)
        {
            _logger = logger;
            this.reportService = reportService;
        }

        public async Task<IActionResult> Index()
        {
            var mostDownloaded = await this.reportService.GetFiveMostDownloadedReportsAsync();
            var featured = await this.reportService.GetFeaturedReportsAsync();
            var newest = await this.reportService.GetFiveNewestReportsAsync();

            var homeViewModel = new HomeViewModel
            {
                Featured = featured.Select(r => new ReportViewModel(r)).ToList(),
                MostDownloaded = mostDownloaded.Select(r => new ReportViewModel(r)).ToList(),
                Newest = newest.Select(r => new ReportViewModel(r)).ToList()
            };

            return View(homeViewModel);
        }

        public IActionResult PageNotFound()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
