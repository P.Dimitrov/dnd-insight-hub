﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A class that maps TagDTO into Tag ViewModel.
    /// </summary>
    public class TagViewModel
    {
        public TagViewModel()
        {

        }

        public TagViewModel(TagDTO tagDTO)
        {
            Id = tagDTO.Id;
            Name = tagDTO.Name;
        }

        public int Id { get; set; }

        public string Name { get; set; }
    }
}
