﻿using InsightHub.Models;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A class that maps ReportDTO into Report ViewModel.
    /// </summary>
    public class ReportViewModel
    {
        public ReportViewModel()
        {
            TagsId = new List<int>();
        }
        public ReportViewModel(ReportDTO reportDTO)
        {

            Id = reportDTO.Id;
            Name = reportDTO.Name;
            Description = reportDTO.Description;
            IndustryID = reportDTO.Industry.Id;
            Industry = reportDTO.Industry;
            Author = reportDTO.Author;
            FileName = reportDTO.FileName;
            TagsId = reportDTO.Tags.Select(tagId => tagId.Id).ToList();
            TimesDownloaded = reportDTO.TimesDownloaded;
            File = reportDTO.File;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IndustryID { get; set; }
        public IndustryDTO Industry { get; set; }
        public int AuthorID { get; set; }
        public string Author { get; set; }
        public ICollection<int> TagsId { get; set; }
        public string FileName { get; set; }
        public int TimesDownloaded { get; set; }
        public IFormFile File { get; set; }
    }
}
