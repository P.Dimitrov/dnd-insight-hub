﻿using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A class that maps ReportDTO into Input Report ViewModel.
    /// </summary>
    public class InputReportModel
    {
        public InputReportModel()
        {
            TagsId = new List<int>();
        }
        public InputReportModel(ReportDTO reportDTO)
        {
            Id = reportDTO.Id;
            Name = reportDTO.Name;
            Description = reportDTO.Description;
            IndustryId = reportDTO.Industry.Id;
            AuthorId = reportDTO.AuthorID;
            TagsId = reportDTO.Tags.Select(tagId => tagId.Id).ToList();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IndustryId { get; set; }
        public int AuthorId { get; set; }
        public ICollection<int> TagsId { get; set; }
        public IFormFile File { get; set; }
    }
}
