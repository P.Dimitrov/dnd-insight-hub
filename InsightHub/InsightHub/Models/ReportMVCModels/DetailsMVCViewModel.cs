﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace InsightHub.Web.Models.ReportMVCModels
{
    /// <summary>
    /// A class that contains properties for details Report ViewModel.
    /// </summary>
    public class DetailsMVCViewModel
    {

            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public int IndustryID { get; set; }
            public IndustryViewModel Industry { get; set; }
            public int AuthorID { get; set; }
            public string Author { get; set; }
            public ICollection<TagViewModel> Tags { get; set; }
            public int TimesDownloaded { get; set; }
            public string FileName { get; set; }
    }
}
