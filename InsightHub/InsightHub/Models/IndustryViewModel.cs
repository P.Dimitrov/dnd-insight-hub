﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InsightHub.Web.Models
{
    /// <summary>
    /// A class that maps IndustryDTO into Industry ViewModel.
    /// </summary>
    public class IndustryViewModel
    {
        public IndustryViewModel()
        {

        }

        public IndustryViewModel(IndustryDTO industryDTO)
        {
            Id = industryDTO.Id;
            Name = industryDTO.Name;
        }

        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

    }
}
