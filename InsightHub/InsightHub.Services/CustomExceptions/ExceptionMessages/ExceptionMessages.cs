﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.CustomExceptions.ExceptionMessages
{
    /// <summary>
    /// A class that procides custom messages for thrown exceptions.
    /// </summary>
    public static class ExceptionMessages
    {
        public const string GeneralOopsMessage = "Oops, something went wrong!";

        public const string EntityNull = "No entity found.";
        public const string DtoEntityNull = "No DTO found.";
        public const string VMEntityNull = "No View Model found.";

        public const string IndustryNull = "No industry found.";
        public const string ReportNull = "No report found.";
        public const string TagNull = "No tag found";
        public const string UserNull = "No user found";
        public const string SubscriptionNull = "No subscription found";

        public const string ModelError = "Model state not valid.";
    }
}
