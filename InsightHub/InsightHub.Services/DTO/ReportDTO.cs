﻿using InsightHub.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

namespace InsightHub.Services.DTO
{
    /// <summary>
    /// A class that maps the Report model into Report DTO.
    /// </summary>
    public class ReportDTO
    {
        public ReportDTO()
        {

        }
        public ReportDTO(Report report)
        {
            Id = report.Id;
            Name = report.Name;
            Description = report.Description;
            IndustryID = report.IndustryID;
            IndustryName = report.Industry.Name;
            Industry = new IndustryDTO(report.Industry);
            AuthorID = report.AuthorID;
            Author = report.Author.UserName;
            FileName = report.Content;
            CreatedOn = report.CreatedOn;
            ChangedOn = report.ChangedOn;
            ApprovedOn = report.ApprovedOn;
            IsApproved = report.IsApproved;
            IsFeatured = report.IsFeatured;
            Tags = report.ReportTags.Select(tag => new TagDTO { Id = tag.TagId, Name = tag.Tag.Name }).ToList();
            TimesDownloaded = report.TimesDownloaded;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int IndustryID { get; set; }
        public string IndustryName { get; set; }
        public IndustryDTO Industry { get; set; }
        public int AuthorID { get; set; }
        public string Author { get; set; }
        public ICollection<TagDTO> Tags { get; set; }
        public IFormFile File { get; set; }
        [JsonIgnore]
        public string FileName { get; set; }
        [JsonIgnore]
        public DateTime CreatedOn { get; set; }
        [JsonIgnore]
        public DateTime? ChangedOn { get; set; }
        [JsonIgnore]
        public bool IsApproved { get; set; }
        [JsonIgnore]
        public DateTime? ApprovedOn { get; set; }
        [JsonIgnore]
        public bool IsFeatured { get; set; }
        [JsonIgnore]
        public int TimesDownloaded { get; set; }
    }

}
