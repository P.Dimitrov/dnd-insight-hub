﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.CustomExceptions.ExceptionMessages;
using InsightHub.Services.DTO;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services
{
    /// <summary>
    ///  A class that implements functionalities for Tag model - 
    ///   - GetTag
    ///    - GetTagByName
    ///     - GetAllTags
    ///      - CreateTag
    ///       - DeleteTag
    ///        - UpdateTag
    /// </summary>
    public class TagService : ITagService
    {
        private readonly InsightHubContext context;

        public TagService(InsightHubContext context)
        {
            this.context = context;
        }

        public async Task<TagDTO> GetTagAsync(int id)
        {
            var tag = await this.context.Tags
                .Include(r => r.ReportTags)
                .ThenInclude(r => r.Report)
                .FirstOrDefaultAsync(t => t.Id == id);

            if (tag == null)
            {
                throw new BusinessLogicException(ExceptionMessages.TagNull);
            }

            var tagDTO = new TagDTO(tag);

            return tagDTO;
        }

        public async Task<TagDTO> GetTagByNameAsync(string name)
        {
            var tag = await this.context.Tags
                .Include(r => r.ReportTags)
                .ThenInclude(r => r.Report)
                .FirstOrDefaultAsync(t => t.Name == name);

            if (tag == null)
            {
                throw new BusinessLogicException(ExceptionMessages.TagNull);
            }

            var tagDTO = new TagDTO(tag);

            return tagDTO;
        }

        public async Task<IEnumerable<TagDTO>> GetAllTagsAsync()
        {
            var allTags = await this.context.Tags
                .ToListAsync();

            if (!allTags.Any())
            {
                throw new BusinessLogicException(ExceptionMessages.TagNull);
            }

            var allTagsDTOs = allTags.Select(tag => new TagDTO
            {
                Id = tag.Id,
                Name = tag.Name
            });

            return allTagsDTOs;
        }
        public async Task<TagDTO> CreateTagAsync(TagDTO tagDTO)
        {
                if (tagDTO == null || tagDTO.Name == null)
                {
                    throw new BusinessLogicException(ExceptionMessages.TagNull);
                }

                var tag = new Tag
                {
                    Name = tagDTO.Name
                };

                await this.context.Tags.AddAsync(tag);
                await this.context.SaveChangesAsync();

                return tagDTO;
            
        }
        public async Task<TagDTO> DeleteTagAsync(int id)
        {
            var tag = await this.context.Tags
                .FirstOrDefaultAsync(b => b.Id == id);

            if (tag == null)
            {
                throw new BusinessLogicException(ExceptionMessages.TagNull);
            }

            this.context.Remove(tag);
            await this.context.SaveChangesAsync();
            var tagDTO = new TagDTO(tag);

            return tagDTO;
        }
        public async Task<TagDTO> UpdateTagAsync(int id, TagDTO tagDTO)
        {
            var tag = await this.context.Tags
                 .FirstOrDefaultAsync(b => b.Id == id);

            if (tag == null)
            {
                throw new BusinessLogicException(ExceptionMessages.TagNull);
            }

            try
            {
                tag.Name = tagDTO.Name;

                this.context.Update(tag);
                await this.context.SaveChangesAsync();
                var editedTagDTO = new TagDTO(tag);

                return editedTagDTO;
            }
            catch (Exception)
            {
                throw new BusinessLogicException(ExceptionMessages.TagNull);
            }
        }
    }
}
