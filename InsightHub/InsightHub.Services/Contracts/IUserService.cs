﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the User Service to access properties of the class.
    /// </summary>
    public interface IUserService
    {
        Task<UserDTO> GetUserAsync(int? id);
        Task<UserDTO> GetUserByNameAsync(string name);
        Task<IEnumerable<UserDTO>> GetAllUsersAsync();
        Task<IEnumerable<UserDTO>> GetAllApprovedUsersAsync();
        Task<IEnumerable<UserDTO>> GetAllPendingUsersAsync();
        Task<bool> DeleteUserAsync(int id);
        Task<UserDTO> ApproveUserAsync(int id);
        Task<UserDTO> BanUserAsync(int id);
    }
}
