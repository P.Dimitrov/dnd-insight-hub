﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the Tag Service to access properties of the class.
    /// </summary>
    public interface ITagService
    {
        Task<TagDTO> GetTagAsync(int id);
        Task<TagDTO> GetTagByNameAsync(string name);
        Task<IEnumerable<TagDTO>> GetAllTagsAsync();
        Task<TagDTO> CreateTagAsync(TagDTO tagDTO);
        Task<TagDTO> DeleteTagAsync(int id);
        Task<TagDTO> UpdateTagAsync(int id, TagDTO tagDTO);
    }
}
