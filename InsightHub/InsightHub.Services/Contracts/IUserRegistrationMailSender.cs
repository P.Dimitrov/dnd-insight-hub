﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IUserRegistrationMailSender
    {
        public Task SendUserRegistrationEmailAsync(string email);
    }
}
