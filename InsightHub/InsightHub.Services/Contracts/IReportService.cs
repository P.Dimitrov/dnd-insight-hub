﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the Report Service to access properties of the class.
    /// </summary>
    public interface IReportService
    {
        Task<ReportDTO> GetReportAsync(int? id);
        Task<ICollection<ReportDTO>> GetAllReportsAsync();
        Task<ReportDTO> CreateReportAsync(ReportDTO reportDTO);
        Task<bool> DeleteReportAsync(int id);
        Task<ReportDTO> UpdateReportAsync(int id, ReportDTO reportDTO);
        //IEnumerable<ReportDTO> Search(string input);
        //Task<IEnumerable<ReportDTO>> SortReportsByAsync(string category);
        Task<ICollection<ReportDTO>> GetFiveNewestReportsAsync();
        Task<ICollection<ReportDTO>> GetFeaturedReportsAsync();
        Task<ICollection<ReportDTO>> GetFiveMostDownloadedReportsAsync();
        Task<ICollection<ReportDTO>> GetAllPendingReportsAsync();
        Task<ICollection<ReportDTO>> GetAllApprovedReportsAsync();
        Task<ICollection<ReportDTO>> GetAllUserDownloadedReportsAsync(string userName);
        Task<ICollection<ReportDTO>> GetAllAuthoredReportsAsync(string userName);
        Task<ReportDTO> ApproveReportAsync(int id);
        Task<ReportDTO> UnfeatureReportAsync(int id);
        Task<ReportDTO> FeatureReportAsync(int id);
        Task<MemoryStream> DownloadReportAsync(int id, int userID);
    }
}
