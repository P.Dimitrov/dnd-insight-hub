﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    public interface IUserActivationMailSender
    {
        public Task SendUserActivationEmailAsync(string email);
    }
}
