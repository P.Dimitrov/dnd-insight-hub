﻿using InsightHub.Services.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services.Contracts
{
    /// <summary>
    /// Interface for the Industry Service to access properties of the class.
    /// </summary>
    public interface IIndustryService
    {
        Task<UserIndustryDTO> GetIndustryAsync(int id);

        Task<IndustryDTO> GetIndustryByNameAsync(string industryName);

        Task<IEnumerable<UserIndustryDTO>> GetAllIndustriesAsync();

        Task<IndustryDTO> CreateIndustryAsync(IndustryDTO industryDTO);

        Task<IndustryDTO> UpdateIndustryAsync(IndustryDTO industryDTO);

        Task<UserIndustryDTO> SubscribeAsync(string userName, int industryId);

        Task<UserIndustryDTO> UnsubscribeAsync(string userName, int industryId);

        Task<IEnumerable<IndustryDTO>> GetAllUserSubscriptionsAsync(string userName);

        Task<bool> DeleteIndustryAsync(int id);
    }
}
