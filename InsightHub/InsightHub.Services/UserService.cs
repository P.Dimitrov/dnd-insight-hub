﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.CustomExceptions.ExceptionMessages;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Services
{
    /// <summary>
    /// A class that implements functionalities for User model - 
    ///  - GetAllPendingUsers
    ///   - GetUser
    ///    - GetUserByName
    ///     - GetUserByName
    ///      - GetUserByName
    ///       - GetAllUsers
    ///        - ApproveUser
    ///         - BanUserAsync
    ///          - GetAllApprovedUsers
    ///           - DeleteUser
    /// </summary>

    public class UserService : IUserService
    {
        private readonly InsightHubContext context;
        private readonly IReportService reportService;
        private readonly IUserActivationMailSender emailSender;

        public UserService(InsightHubContext context, IReportService reportService, IUserActivationMailSender emailSender)
        {
            this.context = context;
            this.reportService = reportService;
            this.emailSender = emailSender;
        }

        public async Task<IEnumerable<UserDTO>> GetAllPendingUsersAsync()
        {
            var users = await this.context.Users
                .Where(u => u.IsApproved == false)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .Select(u => new UserDTO(u)).ToListAsync();

            return users;
        }

        public async Task<UserDTO> GetUserAsync(int? id)
        {
            var user = await this.context.Users
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Author)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Author)
                .FirstOrDefaultAsync(i => i.Id == id);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var userDTO = new UserDTO(user);

            return userDTO;
        }

        public async Task<UserDTO> GetUserByNameAsync(string name)
        {
            var user = await this.context.Users
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Author)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Author)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .FirstOrDefaultAsync(i => i.UserName == name);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var userDTO = new UserDTO(user);

            return userDTO;
        }

        public async Task<IEnumerable<UserDTO>> GetAllUsersAsync()
        {
            var users = await this.context.Users
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Author)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Author)
                .Select(u => new UserDTO(u)).ToListAsync();

            return users;
        }

        public async Task<UserDTO> ApproveUserAsync(int id)
        {
            var user = await this.context.Users
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .FirstOrDefaultAsync(i => i.Id == id);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            user.IsApproved = true;
            user.ApprovedOn = DateTime.UtcNow;
            user.ChangedOn = DateTime.UtcNow;

            await this.context.SaveChangesAsync();

            await this.emailSender.SendUserActivationEmailAsync(user.UserName);

            var userDtoToReturn = new UserDTO(user);

            return userDtoToReturn;
        }

        public async Task<UserDTO> BanUserAsync(int id)
        {
            var user = await this.context.Users
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Author)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Author)
                .FirstOrDefaultAsync(i => i.Id == id);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            user.IsApproved = false;
            user.ApprovedOn = null;
            user.ChangedOn = DateTime.UtcNow;

            await this.context.SaveChangesAsync();

            var userDtoToReturn = new UserDTO(user);

            return userDtoToReturn;
        }

        public async Task<IEnumerable<UserDTO>> GetAllApprovedUsersAsync()
        {
            var users = await this.context.Users
                .Where(u => u.IsApproved == true)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Author)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Author)
                .Select(u => new UserDTO(u)).ToListAsync();

            return users;
        }

        public async Task<bool> DeleteUserAsync(int id)
        {
            var user = await this.context.Users
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Industry)
                .Include(u => u.AuthoredReports)
                .ThenInclude(ar => ar.Author)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.ReportTags)
                .ThenInclude(rt => rt.Tag)
                .Include(u => u.Subscriptions)
                .ThenInclude(u => u.Industry)
                .Include(u => u.DownloadedReports)
                .ThenInclude(dr => dr.Report)
                .ThenInclude(r => r.Author)
                .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            if (user.AuthoredReports != null)
            {
                foreach (var report in user.AuthoredReports)
                {
                    await this.reportService.DeleteReportAsync(report.Id);
                }
            }

            if (user.Subscriptions != null)
            {
                this.context.UserIndustries.RemoveRange(user.Subscriptions);
            }

            this.context.Users.Remove(user);
            await this.context.SaveChangesAsync();

            return true;
        }
    }
}
