﻿using InsightHub.Services.Providers.Contracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace InsightHub.Services.Providers
{
    /// <summary>
    /// A class that implements functionalities for File Type Provider - gets the file extension from the file name. 
    /// </summary>
    public class FileTypeProvider : IFileTypeProvider
    {
        private readonly Dictionary<string, string> extensionTypes = new Dictionary<string, string>
        {
                { ".pdf", "application/pdf" }
        };

        public string GetFileType(string fileName)
        {
            var ext = Path.GetExtension(fileName).ToLowerInvariant();

            return extensionTypes[ext];
        }
    }
}
