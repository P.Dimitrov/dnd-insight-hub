﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Services.Providers.Contracts
{
    /// <summary>
    /// Interface for the FileType Provider  to access properties of the class.
    /// </summary>
    public interface IFileTypeProvider
    {
        string GetFileType(string fileName);
    }
}
