﻿using InsightHub.Services.Contracts;
using InsightHub.Services.DTO;
using InsightHub.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using InsightHub.Models;
using System.ComponentModel.Design.Serialization;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.CustomExceptions.ExceptionMessages;

namespace InsightHub.Services
{
    /// <summary>
    /// A class that implements functionalities for Industry model - 
    ///  - GetIndustry
    ///   - GetIndustryByName
    ///    - GetAllIndustries
    ///     - CreateIndustry
    ///      - DeleteIndustry
    ///       - UpdateIndustry
    ///        - Subscribe
    ///         - Unsubscribe
    ///          - GetAllUserSubscriptions
    /// </summary> 
    public class IndustryService : IIndustryService
    {
        private readonly InsightHubContext context;

        public IndustryService(InsightHubContext context)
        {
            this.context = context;
        }

        public async Task<UserIndustryDTO> GetIndustryAsync(int id)
        {
            var industry = await this.context.Industries
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .FirstOrDefaultAsync(i => i.Id == id);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }

            var industryDTO = new UserIndustryDTO(industry);

            return industryDTO;
        }

        public async Task<IndustryDTO> GetIndustryByNameAsync(string industryName)
        {
            var industry = await this.context.Industries
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .FirstOrDefaultAsync(i => i.Name == industryName);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }

            var industryDTO = new IndustryDTO(industry);

            return industryDTO;
        }

        public async Task<IEnumerable<UserIndustryDTO>> GetAllIndustriesAsync()
        {
            var industries = await this.context.Industries
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .Select(i => new UserIndustryDTO(i)).ToListAsync();

            return industries;
        }

        public async Task<IndustryDTO> CreateIndustryAsync(IndustryDTO industryDTO)
        {
            if (industryDTO == null || industryDTO.Name == null)
            {
                throw new BusinessLogicException(ExceptionMessages.DtoEntityNull);
            }

            var industry = new Industry
            {
                Name = industryDTO.Name
            };

            await this.context.Industries.AddAsync(industry);
            await this.context.SaveChangesAsync();

            var returnDTO = new IndustryDTO(industry);

            return returnDTO;
        }

        public async Task<bool> DeleteIndustryAsync(int id)
        {
            var industry = await this.context.Industries
                .FirstOrDefaultAsync(i => i.Id == id);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }


            var subscriptions = await this.context.UserIndustries
                .Where(ui => ui.IndustryId == id).ToListAsync();

            if (subscriptions != null)
            {
                this.context.UserIndustries.RemoveRange(subscriptions);
            }

            var reports = await this.context.Reports
                .Where(r => r.IndustryID == id).ToListAsync();

            if (reports != null)
            {
                foreach (var report in reports)
                {
                    report.IndustryID = 11;
                }
            }

            this.context.UpdateRange(reports);
            this.context.Industries.Remove(industry);
            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<IndustryDTO> UpdateIndustryAsync(IndustryDTO industryDTO)
        {
            if (industryDTO == null)
            {
                throw new BusinessLogicException(ExceptionMessages.DtoEntityNull);
            }

            var industry = await this.context.Industries
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .FirstOrDefaultAsync(i => i.Id == industryDTO.Id);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }

            industry.Name = industryDTO.Name;

            this.context.Update(industry);
            await this.context.SaveChangesAsync();

            var returnDTO = new IndustryDTO(industry);

            return returnDTO;
        }

        public async Task<UserIndustryDTO> SubscribeAsync(string userName, int industryId)
        {
            if (userName == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ModelError);
            }

            var industry = await this.context.Industries
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .FirstOrDefaultAsync(i => i.Id == industryId);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }

            var user = await this.context.Users
                .FirstOrDefaultAsync(i => i.UserName == userName);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var subscription = new UserIndustry
            {
                IndustryId = industry.Id,
                UserId = user.Id
            };

            await this.context.UserIndustries.AddAsync(subscription);

            await this.context.SaveChangesAsync();

            var returnDTO = new UserIndustryDTO(industry);

            return returnDTO;
        }

        public async Task<UserIndustryDTO> UnsubscribeAsync(string userName, int industryId)
        {
            if (userName == null)
            {
                throw new BusinessLogicException(ExceptionMessages.ModelError);
            }

            var industry = await this.context.Industries
                .Include(i => i.Subscribers)
                .ThenInclude(ui => ui.User)
                .FirstOrDefaultAsync(i => i.Id == industryId);

            if (industry == null)
            {
                throw new BusinessLogicException(ExceptionMessages.IndustryNull);
            }

            var user = await this.context.Users
                .FirstOrDefaultAsync(i => i.UserName == userName);

            if (user == null)
            {
                throw new BusinessLogicException(ExceptionMessages.UserNull);
            }

            var subscription = await this.context.UserIndustries
                .FirstOrDefaultAsync(ui => ui.IndustryId == industry.Id && ui.UserId == user.Id);

            if (subscription == null)
            {
                throw new BusinessLogicException(ExceptionMessages.SubscriptionNull);
            }


            this.context.Remove(subscription);
            await this.context.SaveChangesAsync();

            var returnDTO = new UserIndustryDTO(industry);

            return returnDTO;
        }

        public async Task<IEnumerable<IndustryDTO>> GetAllUserSubscriptionsAsync(string userName)
        {
            var industries = await this.context.UserIndustries
                .Include(ui => ui.User)
                .Include(ui => ui.Industry)
                .Where(ui => ui.User.UserName == userName)
                .Select(i => new IndustryDTO(i.Industry)).ToListAsync();

            return industries;
        }
    }
}
