﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Models.Contracts
{
    /// <summary>
    /// An interface that contains properties for creating users and reports.
    /// </summary>
    public interface IAuditable
    {
        public DateTime CreatedOn { get; set; }

        public DateTime? ChangedOn { get; set; }

    }
}
