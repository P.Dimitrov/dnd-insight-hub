﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Models.Contracts
{
    /// <summary>
    /// An interface that contains properties for approving users and reports.
    /// </summary>
    public interface IApprovable
    {
        public DateTime? ApprovedOn { get; set; }

        public bool IsApproved { get; set; }
    }
}
