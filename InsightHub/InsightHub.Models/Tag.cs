﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that contains the properties of Tag entity.
    /// </summary>
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<ReportTag> ReportTags { get; set; }
    }
}
