﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that contains the properties of ReportTag entity.
    /// </summary>
    public class ReportTag
    {
        public int ReportId { get; set; }
        public Report Report { get; set; }
        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
