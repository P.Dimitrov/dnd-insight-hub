﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that contains the properties of UserReport entity.
    /// </summary>
    public class UserReport
    {
        public int UserId { get; set; }

        public User User { get; set; }

        public int ReportId { get; set; }

        public Report Report { get; set; }
    }
}
