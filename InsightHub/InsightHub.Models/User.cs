﻿using InsightHub.Models.Contracts;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace InsightHub.Models
{
    /// <summary>
    /// A class that contains the properties of User entity. It implements IdentityUser, IAuditable and IApprovable.
    /// </summary>
    public class User : IdentityUser<int>, IAuditable, IApprovable
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public ICollection<Report> AuthoredReports { get; set; }

        public ICollection<UserReport> DownloadedReports { get; set; }

        public ICollection<UserIndustry> Subscriptions { get; set; }

        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;

        public DateTime? ChangedOn { get; set; }

        public DateTime? ApprovedOn { get; set; }

        public bool IsApproved { get; set; }
    }
}
