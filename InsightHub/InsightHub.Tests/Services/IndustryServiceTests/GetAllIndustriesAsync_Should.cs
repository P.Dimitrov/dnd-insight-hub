﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.IndustryServiceTests
{
    [TestClass]
    public class GetAllIndustriesAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectIndusties_When_Called()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ReturnCorrectIndusties_When_Called));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };
            var industry2 = new Industry
            {
                Name = "Retail"
            };
            var industry3 = new Industry
            {
                Name = "Healthcare"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.Industries.AddAsync(industry2);
                await arrangeContext.Industries.AddAsync(industry3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var industries = new List<Industry> { industry1, industry2, industry3 };
                var sut = new IndustryService(assertContext);
                var result = await sut.GetAllIndustriesAsync();
                var resultList = result.ToList();

                Assert.AreEqual(industries[0].Name, resultList[0].Name);
                Assert.AreEqual(industries[0].Id, resultList[0].Id);
                Assert.AreEqual(industries[1].Name, resultList[1].Name);
                Assert.AreEqual(industries[1].Id, resultList[1].Id);
                Assert.AreEqual(industries[2].Name, resultList[2].Name);
                Assert.AreEqual(industries[2].Id, resultList[2].Id);
            }
        }
    }
}
