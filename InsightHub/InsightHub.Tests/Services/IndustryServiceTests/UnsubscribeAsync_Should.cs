﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.CustomExceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.IndustryServiceTests
{
    [TestClass]
    public class UnsubscribeAsync_Should
    {
        [TestMethod]
        public async Task UnsubscribeUser_When_ParamsAreCorrect()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(UnsubscribeUser_When_ParamsAreCorrect));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            var user1 = new User
            {
                UserName = "John",
            };

            var subscription = new UserIndustry
            {
                IndustryId = 1,
                UserId = 1
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.UserIndustries.AddAsync(subscription);
                await arrangeContext.SaveChangesAsync();
            }

            //Arrange & Act

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                Assert.IsTrue(assertContext.UserIndustries.Count() == 1);

                var result = await sut.UnsubscribeAsync("John", industry1.Id);

                Assert.IsFalse(result.Subscribers.Contains("John"));
                Assert.IsTrue(assertContext.UserIndustries.Count() == 0);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowsException_When_UserIsNotFound()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowsException_When_UserIsNotFound));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            var subscription = new UserIndustry
            {
                IndustryId = 1,
                UserId = 1
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.SaveChangesAsync();
            }

            //Arrange & Act

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.UnsubscribeAsync("John", industry1.Id);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowsException_When_IndustryIsNotFound()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowsException_When_IndustryIsNotFound));

            var user1 = new User
            {
                UserName = "John",
            };

            var subscription = new UserIndustry
            {
                IndustryId = 1,
                UserId = 1
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.SaveChangesAsync();
            }

            //Arrange & Act

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.UnsubscribeAsync("John", 1);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowsException_When_UserNameIsNotValid()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowsException_When_UserNameIsNotValid));

            var user1 = new User
            {
                UserName = "John",
            };

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            var subscription = new UserIndustry
            {
                IndustryId = 1,
                UserId = 1
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.SaveChangesAsync();
            }

            //Arrange & Act

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.UnsubscribeAsync(null, 1);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowsException_When_SubscriptionIsNotFound()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowsException_When_SubscriptionIsNotFound));

            var user1 = new User
            {
                UserName = "John",
            };

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.Users.AddAsync(user1);
                await arrangeContext.SaveChangesAsync();
            }

            //Arrange & Act

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var result = await sut.UnsubscribeAsync("John", 1);
            }
        }
    }
}
