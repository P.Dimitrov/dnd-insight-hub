﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.IndustryServiceTests
{
    [TestClass]
    public class DeleteIndustryAsync_Should
    {
        [TestMethod]
        public async Task DeleteIndustry_When_ValidParamsGiven()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(DeleteIndustry_When_ValidParamsGiven));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var deleted = await sut.DeleteIndustryAsync(1);

                Assert.IsTrue(deleted);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.GetIndustryAsync(1));
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowException_When_IndustryNotFound()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowException_When_IndustryNotFound));

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);
                var deleted = await sut.DeleteIndustryAsync(1);
            }
        }

        [TestMethod]
        public async Task DeletesUserSubscriptions_When_IndustryIsDeleted()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(DeletesUserSubscriptions_When_IndustryIsDeleted));

            var industry1 = new Industry
            {
                Name = "Financial Services"
            };

            var subscription1 = new UserIndustry
            {
                IndustryId = 1,
                UserId = 2
            };
            var subscription2 = new UserIndustry
            {
                IndustryId = 1,
                UserId = 1
            };
            var subscription3 = new UserIndustry
            {
                IndustryId = 1,
                UserId = 3
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry1);
                await arrangeContext.UserIndustries.AddRangeAsync(subscription1, subscription2, subscription3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new IndustryService(assertContext);

                Assert.AreEqual(3, assertContext.UserIndustries.Count());

                var deleted = await sut.DeleteIndustryAsync(1);

                Assert.IsTrue(deleted);
                Assert.AreEqual(0, assertContext.UserIndustries.Count());
            }
        }
    }
}
