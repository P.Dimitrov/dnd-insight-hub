﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.TagServiceTests
{
    [TestClass]
    public class GetTagByNameAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTag_When_ValidNameIsGiven()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectTag_When_ValidNameIsGiven));

            var tag = new Tag
            {
                Name = "Media"
            };
            var tag2 = new Tag
            {
                Name = "IT Services"
            };
            var tag3 = new Tag
            {
                Name = "Finance"
            };
            var tagDTO = new TagDTO
            {
                Name = "Media"
            };
            var tagDTO2 = new TagDTO
            {
                Name = "IT Services"
            };
            var tagDTO3 = new TagDTO
            {
                Name = "Finance"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Tags.AddAsync(tag2);
                await arrangeContext.Tags.AddAsync(tag3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var result = await sut.GetTagByNameAsync("Finance");

                Assert.AreEqual(tag3.Id, result.Id);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_TagNotFoundByName()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_TagNotFoundByName));

            var tag2 = new Tag
            {
                Name = "IT Services"
            };

            var tagDTO2 = new TagDTO
            {
                Name = "IT Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag2);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new TagService(assertContext);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.GetTagByNameAsync("Travel"));
            }
        }
    }
}
