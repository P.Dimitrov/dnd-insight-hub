﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.TagServiceTests
{
    [TestClass]
    public class UpdateTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTag_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectTag_When_ParamsAreValid));

            var tag = new Tag
            {
                Name = "Media"
            };
            
            var tagDTO = new TagDTO
            {
                Id = 1,
                Name = "Media"
            };
            var newTagDTO = new TagDTO
            {
                Id = 1,
                Name = "IT Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var result = await sut.UpdateTagAsync(1, newTagDTO);

                Assert.AreEqual(newTagDTO.Id, result.Id);
                Assert.AreEqual(newTagDTO.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_UpdatedTagNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_UpdatedTagNotFound));

            var tag = new Tag
            {
                Name = "Media"
            };

            var tagDTO = new TagDTO
            {
                Name = "Media"
            };
            var newTagDTO = new TagDTO
            {
                Name = "IT Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new TagService(assertContext);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(() => sut.UpdateTagAsync(2, newTagDTO));
            }
        }
    }
}
