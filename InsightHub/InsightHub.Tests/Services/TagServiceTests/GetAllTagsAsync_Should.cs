﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.TagServiceTests
{
    [TestClass]
    public class GetAllTagsAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectTags_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectTags_When_ParamsAreValid));

            var tag = new Tag
            {
                Name = "Media"
            };
            var tag2 = new Tag
            {
                Name = "IT Services"
            };
            var tag3 = new Tag
            {
                Name = "Finance"
            };
            var tagDTO = new TagDTO
            {
                Name = "Media"
            };
            var tagDTO2 = new TagDTO
            {
                Name = "IT Services"
            };
            var tagDTO3 = new TagDTO
            {
                Name = "Finance"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.Tags.AddAsync(tag2);
                await arrangeContext.Tags.AddAsync(tag3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var result = await sut.GetAllTagsAsync();
                List<TagDTO> resultList = result.Select(t => t).ToList();

                Assert.AreEqual(tag.Name, resultList[0].Name);
                Assert.AreEqual(tag2.Name, resultList[1].Name);
                Assert.AreEqual(tag3.Name, resultList[2].Name);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NoTagIsFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NoTagIsFound));

            ///Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);

                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.GetAllTagsAsync());
            }
        }
    }
}
