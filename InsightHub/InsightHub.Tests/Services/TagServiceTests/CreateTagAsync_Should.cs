﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.TagServiceTests
{
    [TestClass]
    public class CreateTagAsync_Should
    {
            [TestMethod]
            public async Task ReturnCorrectTypeTag_When_ParamsAreValid()
            {
                var options = Utilities.GetOptions(nameof(ReturnCorrectTypeTag_When_ParamsAreValid));

            var tag = new Tag
            {
                Name = "IT Services"
            };

            var tagDTO = new TagDTO
            {
                Name = "IT Services"
            };


            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
                {
                    var sut = new TagService(assertContext);
                    var expectedType = new TagDTO();
                    var result = await sut.CreateTagAsync(tagDTO);

                    Assert.IsInstanceOfType(result, typeof(TagDTO));
                }
            }

            [TestMethod]
            public async Task ReturnCorrectTag_When_ParamsAreValid()
            {
                var options = Utilities.GetOptions(nameof(ReturnCorrectTag_When_ParamsAreValid));

            var tag = new Tag
            {
                Name = "IT Services"
            };

            var tagDTO = new TagDTO
            {
                Id = 1,
                Name = "IT Services"
            };

                //Act & Assert
                using (var assertContext = new InsightHubContext(options))
                {
                    var sut = new TagService(assertContext);
                    var expected = new TagDTO
                    {
                        Id = 1,
                        Name = "IT Services"
                    };
                    var result = await sut.CreateTagAsync(tagDTO);

                    Assert.AreEqual(expected.Id, result.Id);
                    Assert.AreEqual(expected.Name, result.Name);
                }
            }

        [TestMethod]
        public async Task ThrowWhen_NullTagIsGiven()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NullTagIsGiven));


            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new TagService(assertContext);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.CreateTagAsync(null));
            }
        }

    }
}
