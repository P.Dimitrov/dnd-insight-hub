﻿using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.TagServiceTests
{
    [TestClass]
    public class DeleteTagAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectDTO_When_TagIsDeleted()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectDTO_When_TagIsDeleted));

            var tag = new Tag
            {
                Name = "IT Services"
            };

            var tagDTO = new TagDTO
            {
                Id = 1,
                Name = "IT Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new TagService(assertContext);
                var expected = new TagDTO
                {
                    Id = 1,
                    Name = "IT Services"
                };
                var result = await sut.DeleteTagAsync(1);

                Assert.AreEqual(expected.Id, result.Id);
                Assert.AreEqual(expected.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task Throw_When_TagIsNotFound()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(Throw_When_TagIsNotFound));

            var tag2 = new Tag
            {
                Name = "IT Services"
            };

            var tagDTO2 = new TagDTO
            {
                Name = "IT Services"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Tags.AddAsync(tag2);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new TagService(assertContext);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.DeleteTagAsync(10));
            }
        }
    }
}
