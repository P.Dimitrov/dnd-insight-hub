﻿using Azure.Storage.Blobs;
using InsightHub.Blob.Service;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.UserServiceTests
{
    [TestClass]
    public class GetAllPendingUsersAsync_Should
    {
        [TestMethod]
        public async Task ReturnAllPendingUsers()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ReturnAllPendingUsers));

            var user1 = new User
            {
                UserName = "John",
                IsApproved = false
            };

            var user2 = new User
            {
                UserName = "Jane",
                IsApproved = true
            };

            var user3 = new User
            {
                UserName = "Jack",
                IsApproved = false
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(user1, user2, user3);

                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var users = new List<User> { user1, user3 };
                var tagService = new Mock<TagService>(assertContext);
                var blobServiceClient = new Mock<BlobServiceClient>();
                var blobService = new Mock<BlobService>(blobServiceClient.Object);
                var mockMailSender = new Mock<IEmailSender>();
                var mockIndustryService = new Mock<IIndustryService>();
                var reportService = new Mock<ReportService>(assertContext, tagService.Object, blobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var mockMailSenderService = new Mock<IUserActivationMailSender>();
                var sut = new UserService(assertContext, reportService.Object, mockMailSenderService.Object);

                var result = await sut.GetAllPendingUsersAsync();
                var resultList = result.ToList();

                Assert.AreEqual(users[0].UserName, resultList[0].UserName);
                Assert.AreEqual(users[0].Id, resultList[0].Id);
                Assert.AreEqual(users[1].UserName, resultList[1].UserName);
                Assert.AreEqual(users[1].Id, resultList[1].Id);
            }
        }
    }
}
