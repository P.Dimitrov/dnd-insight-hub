﻿using Azure.Storage.Blobs;
using InsightHub.Blob.Service;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.UserServiceTests
{
    [TestClass]
    public class ApproveUserAsync_Should
    {
        [TestMethod]
        public async Task ApproveUser_If_UserExists()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ApproveUser_If_UserExists));

            var user1 = new User
            {
                UserName = "John",
                IsApproved = false
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Users.AddAsync(user1);

                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var tagService = new Mock<TagService>(assertContext);
                var blobServiceClient = new Mock<BlobServiceClient>();
                var blobService = new Mock<BlobService>(blobServiceClient.Object);
                var mockMailSender = new Mock<IEmailSender>();
                var mockIndustryService = new Mock<IIndustryService>();
                var reportService = new Mock<ReportService>(assertContext, tagService.Object, blobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var mockMailSenderService = new Mock<IUserActivationMailSender>();
                var sut = new UserService(assertContext, reportService.Object, mockMailSenderService.Object);

                var result = await sut.ApproveUserAsync(1);

                Assert.IsTrue(result.IsApproved);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(BusinessLogicException))]
        public async Task ThrowException_If_UserToBeApprovedDoesNotExist()
        {
            //Arrange

            var options = Utilities.GetOptions(nameof(ThrowException_If_UserToBeApprovedDoesNotExist));

            //Act & Assert

            using (var assertContext = new InsightHubContext(options))
            {
                var tagService = new Mock<TagService>(assertContext);
                var blobServiceClient = new Mock<BlobServiceClient>();
                var blobService = new Mock<BlobService>(blobServiceClient.Object);
                var mockMailSender = new Mock<IEmailSender>();
                var mockIndustryService = new Mock<IIndustryService>();
                var reportService = new Mock<ReportService>(assertContext, tagService.Object, blobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var mockMailSenderService = new Mock<IUserActivationMailSender>();
                var sut = new UserService(assertContext, reportService.Object, mockMailSenderService.Object);

                var result = await sut.ApproveUserAsync(1);
            }
        }
    }
}
