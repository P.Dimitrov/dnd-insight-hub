﻿using InsightHub.Blob.Service.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.ReportServiceTests
{
    [TestClass]
    public class ApproveReportAsync_Should
    {
        [TestMethod]
        public async Task ApproveReport_When_ValidParamsAreGiven()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ApproveReport_When_ValidParamsAreGiven));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Id = 3,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 3 } };

            var report = new Report
            {
                Name = "Rech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsApproved = false
            };
            var report2 = new Report
            {
                Name = "Finance Report"
            };
            var report3 = new Report
            {
                Name = "Medical Report"
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.Reports.AddAsync(report2);
                await arrangeContext.Reports.AddAsync(report3);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var result = await sut.ApproveReportAsync(1);

                Assert.IsTrue(result.IsApproved);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_ReportNotFoundToBeApproved()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_ReportNotFoundToBeApproved));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Id = 3,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 3 } };

            var report = new Report
            {
                Name = "Tech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.ApproveReportAsync(10));
            }
        }
    }
}
