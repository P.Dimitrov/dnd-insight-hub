﻿using InsightHub.Blob.Service.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.ReportServiceTests
{
    [TestClass]
    public class UpdateReportAsync_Should
    {
        [TestMethod]
        public async Task ReturnUpdatedReport_When_ValidParamsAreGiven()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnUpdatedReport_When_ValidParamsAreGiven));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "finance"
            };
            var industryDTO = new IndustryDTO
            {
                Id = 1,
                Name = "finance"
            };
            var author = new User
            {
                UserName = "John Doe",
                AuthoredReports = null
            };
            var authorDTO = new UserDTO
            {
                Id = 1,
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Name = "ÏT Services"
            };
            var tagDTO = new TagDTO
            {
                Id = 1,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 1 } };

            var report = new Report
            {
                Name = "Rech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
            };
            var reportDTO = new ReportDTO
            {
                Id = 1,
                Name = "Rech Report",
                Description = "Reports' description",
                Industry = industryDTO,
                IndustryID = industryDTO.Id,
                FileName = "FileName",
                Author = "John Doe",
                AuthorID = 1,
                Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
            };

            var newReportDTO = new ReportDTO
            {
                Id = 1,
                Name = "Tech Report",
                Description = "Reports' description",
                Industry = industryDTO,
                IndustryID = industryDTO.Id,
                IndustryName = "finance",
                FileName = "FileName",
                Author = "John Doe",
                AuthorID = 1,
                Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.Users.AddAsync(author);
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();

            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var expected = new ReportDTO
                {
                    Id = 1,
                    Name = "Tech Report",
                    Description = "Reports' description",
                    Industry = new IndustryDTO
                    {
                        Id = 1,
                        Name = "Finance"
                    },
                    IndustryID = 1,
                    IndustryName = "finance",
                    FileName = "FileName",
                    Author = "John Doe",
                    AuthorID = 1,
                    Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
                };
                var result = await sut.UpdateReportAsync(1, newReportDTO);

                Assert.AreEqual(expected.Name, result.Name);
            }
        }

        [TestMethod]
        public async Task ReturnUpdatedReport_When_NullIndustryIsGiven()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnUpdatedReport_When_NullIndustryIsGiven));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "finance"
            };
            var industryDTO = new IndustryDTO
            {
                Id = 1,
                Name = "finance"
            };
            var newIndustry = new Industry
            {
                Name = "tech"
            };
            var newIndustryDTO = new IndustryDTO
            {
                Id = 2,
                Name = "tech"
            };
            var author = new User
            {
                UserName = "John Doe",
                AuthoredReports = null
            };
            var authorDTO = new UserDTO
            {
                Id = 1,
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Name = "ÏT Services"
            };
            var tagDTO = new TagDTO
            {
                Id = 1,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 1 } };

            var report = new Report
            {
                Name = "Rech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
            };
            var reportDTO = new ReportDTO
            {
                Id = 1,
                Name = "Rech Report",
                Description = "Reports' description",
                Industry = industryDTO,
                IndustryID = industryDTO.Id,
                FileName = "FileName",
                Author = "John Doe",
                AuthorID = 1,
                Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
            };

            var newReportDTO = new ReportDTO
            {
                Id = 1,
                Name = "Tech Report",
                Description = "Reports' description",
                Industry = newIndustryDTO,
                IndustryID = 4,
                FileName = "FileName",
                Author = "John Doe",
                AuthorID = 1,
                Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.Users.AddAsync(author);
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);

                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.UpdateReportAsync(1, newReportDTO));
            }
        }

        [TestMethod]
        public async Task ReturnUpdatedReport_When_NullAuthorIsGiven()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ReturnUpdatedReport_When_NullAuthorIsGiven));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "finance"
            };
            var industryDTO = new IndustryDTO
            {
                Id = 1,
                Name = "finance"
            };
            var author = new User
            {
                UserName = "John Doe",
                AuthoredReports = null
            };
            var authorDTO = new UserDTO
            {
                Id = 1,
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Name = "ÏT Services"
            };
            var tagDTO = new TagDTO
            {
                Id = 1,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 1 } };

            var report = new Report
            {
                Name = "Rech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
            };
            var reportDTO = new ReportDTO
            {
                Id = 1,
                Name = "Rech Report",
                Description = "Reports' description",
                Industry = industryDTO,
                IndustryID = industryDTO.Id,
                FileName = "FileName",
                Author = "John Doe",
                AuthorID = 1,
                Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
            };
            var newReportDTO = new ReportDTO
            {
                Id = 1,
                Name = "Tech Report",
                Description = "Reports' description",
                Industry = industryDTO,
                IndustryID = industryDTO.Id,
                FileName = "FileName",
                Author = "Josh Doe",
                AuthorID = 1,
                Tags = new List<TagDTO> { new TagDTO { Name = tagDTO.Name, Id = tagDTO.Id } }
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Industries.AddAsync(industry);
                await arrangeContext.Tags.AddAsync(tag);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);

                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.UpdateReportAsync(1, newReportDTO));
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NullUpdatedReportDTOIsGiven()
        {
            //Arrange
            var options = Utilities.GetOptions(nameof(ThrowWhen_NullUpdatedReportDTOIsGiven));
            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();


            using (var assertContext = new InsightHubContext(options))
            {
                //Act & Assert
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                await Assert.ThrowsExceptionAsync<BusinessLogicException>(async () => await sut.UpdateReportAsync(10, null));
            }
        }
    }
}
