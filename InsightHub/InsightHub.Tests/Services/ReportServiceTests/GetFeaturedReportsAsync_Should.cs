﻿using InsightHub.Blob.Service.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.ReportServiceTests
{
    [TestClass]
    public class GetFeaturedReportsAsync_Should
    {
        [TestMethod]
        public async Task ReturnFiveFeaturedReports_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnFiveFeaturedReports_When_ParamsAreValid));

            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Id = 3,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 3 } };

            var report = new Report
            {
                Name = "Tech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsFeatured = true
            };
            var report2 = new Report
            {
                Name = "Medical Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsFeatured = true
            };
            var report3 = new Report
            {
                Name = "Finance Report",
                Industry = industry,
                Author = author,
                IsFeatured = true
            };
            var report4 = new Report
            {
                Name = "Government Report",
                Industry = industry,
                Author = author,
                IsFeatured = true
            };
            var report5 = new Report
            {
                Name = "Agriculture Report",
                Industry = industry,
                Author = author,
                IsFeatured = true
            };
            var report6 = new Report
            {
                Name = "Travel Report",
                Industry = industry,
                Author = author,
                IsFeatured = false
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.Reports.AddAsync(report2);
                await arrangeContext.Reports.AddAsync(report3);
                await arrangeContext.Reports.AddAsync(report4);
                await arrangeContext.Reports.AddAsync(report5);
                await arrangeContext.Reports.AddAsync(report6);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var result = await sut.GetFeaturedReportsAsync();
                List<ReportDTO> resultList = result.Select(t => t).ToList();

                Assert.IsTrue(resultList.Count == 5);
            }
        }

        [TestMethod]
        public async Task ReturnCorrectFiveFeaturedReports_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnCorrectFiveFeaturedReports_When_ParamsAreValid));

            var mockTagService = new Mock<ITagService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe"
            };
            var tag = new Tag
            {
                Id = 3,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 3 } };

            var report = new Report
            {
                Name = "Tech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsFeatured = true
            };
            var report2 = new Report
            {
                Name = "Medical Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsFeatured = true
            };
            var report3 = new Report
            {
                Name = "Finance Report",
                Industry = industry,
                Author = author,
                IsFeatured = true
            };
            var report4 = new Report
            {
                Name = "Government Report",
                Industry = industry,
                Author = author,
                IsFeatured = true
            };
            var report5 = new Report
            {
                Name = "Agriculture Report",
                Industry = industry,
                Author = author,
                IsFeatured = true
            };
            var report6 = new Report
            {
                Name = "Travel Report",
                Industry = industry,
                Author = author,
                IsFeatured = false
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddAsync(report);
                await arrangeContext.Reports.AddAsync(report2);
                await arrangeContext.Reports.AddAsync(report3);
                await arrangeContext.Reports.AddAsync(report4);
                await arrangeContext.Reports.AddAsync(report5);
                await arrangeContext.Reports.AddAsync(report6);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var result = await sut.GetFeaturedReportsAsync();
                List<ReportDTO> resultList = result.Select(t => t).ToList();

                Assert.AreEqual(report.Name, resultList[0].Name);
                Assert.AreEqual(report2.Name, resultList[1].Name);
                Assert.AreEqual(report3.Name, resultList[2].Name);
                Assert.AreEqual(report4.Name, resultList[3].Name);
                Assert.AreEqual(report5.Name, resultList[4].Name);
            }
        }
    }
}
