﻿using InsightHub.Blob.Service.Contracts;
using InsightHub.Data;
using InsightHub.Models;
using InsightHub.Services;
using InsightHub.Services.Contracts;
using InsightHub.Services.CustomExceptions;
using InsightHub.Services.DTO;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightHub.Tests.Services.ReportServiceTests
{
    [TestClass]
    public class GetAllUserDownloadedReportsAsync_Should
    {
        [TestMethod]
        public async Task ReturnAllDownloadedReports_When_ParamsAreValid()
        {
            var options = Utilities.GetOptions(nameof(ReturnAllDownloadedReports_When_ParamsAreValid));

            var mockTagService = new Mock<ITagService>();
            var mockUserService = new Mock<IUserService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe",
                IsApproved = true
            };
            var tag = new Tag
            {
                Id = 3,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 3 } };

            var report = new Report
            {
                Name = "Tech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsApproved = true
            };
            var report2 = new Report
            {
                Name = "Medical Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsApproved = true
            };

            var client = new User
            {
                UserName = "Jane Doe",
                IsApproved = true,
                DownloadedReports = new List<UserReport> { new UserReport { Report = report, ReportId = 1 },
                new UserReport { Report = report2, ReportId = 2 }} 
            };
        
            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddRangeAsync(report, report2);
                await arrangeContext.Users.AddRangeAsync(client, author);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var result = await sut.GetAllUserDownloadedReportsAsync("Jane Doe");
                List<ReportDTO> resultList = result.Select(t => t).ToList();

                Assert.IsTrue(resultList.Count == 2);
            }
        }

        [TestMethod]
        public async Task ReturnZeroDownloadedReports_When_NoReportsAreFound()
        {
            var options = Utilities.GetOptions(nameof(ReturnZeroDownloadedReports_When_NoReportsAreFound));

            var mockTagService = new Mock<ITagService>();
            var mockUserService = new Mock<IUserService>();
            var mockBlobService = new Mock<IBlobService>();
            var mockMailSender = new Mock<IEmailSender>();
            var mockIndustryService = new Mock<IIndustryService>();

            var industry = new Industry
            {
                Name = "Finance"
            };
            var author = new User
            {
                UserName = "John Doe",
                IsApproved = true
            };
            var tag = new Tag
            {
                Id = 3,
                Name = "ÏT Services"
            };
            var reportTag = new List<ReportTag> { new ReportTag { Tag = tag, TagId = 3 } };

            var report = new Report
            {
                Name = "Tech Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsApproved = true
            };
            var report2 = new Report
            {
                Name = "Medical Report",
                Industry = industry,
                Author = author,
                ReportTags = reportTag,
                IsApproved = true
            };

            var client = new User
            {
                UserName = "Jane Doe",
                IsApproved = true,
                DownloadedReports = new List<UserReport> { new UserReport { Report = report, ReportId = 1 },
                new UserReport { Report = report2, ReportId = 2 }}
            };

            using (var arrangeContext = new InsightHubContext(options))
            {
                await arrangeContext.Reports.AddRangeAsync(report, report2);
                await arrangeContext.Users.AddRangeAsync(client, author);
                await arrangeContext.SaveChangesAsync();
            }

            //Act & Assert
            using (var assertContext = new InsightHubContext(options))
            {
                var sut = new ReportService(assertContext, mockTagService.Object, mockBlobService.Object, mockMailSender.Object, mockIndustryService.Object);
                var result = await sut.GetAllUserDownloadedReportsAsync("John Doe");
                List<ReportDTO> resultList = result.Select(t => t).ToList();

                Assert.IsTrue(resultList.Count == 0);
            }
        }
    }
}
