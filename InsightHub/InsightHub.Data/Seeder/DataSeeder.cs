﻿using InsightHub.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Seeder
{
    /// <summary>
    /// A class that seeds entities for the models in the database.
    /// </summary>
    public static class DataSeeder
    {
        public static void InitialSeed(this ModelBuilder builder)
        {

            builder.Entity<Role>().HasData(
                new Role
                {
                    Id = 1,
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                },
                new Role
                {
                    Id = 2,
                    Name = "Author",
                    NormalizedName = "Author"
                },
                new Role
                {
                    Id = 3,
                    Name = "Client",
                    NormalizedName = "Client"
                }
            );

            var hasher = new PasswordHasher<User>();

            User adminUser = new User
            {
                Id = 1,
                UserName = "admin@insighthub.com",
                FirstName = "Admin",
                LastName = "Admin",
                NormalizedUserName = "ADMIN@INSIGHTHUB.COM",
                Email = "admin@insighthub.com",
                NormalizedEmail = "ADMIN@INSIGHTHUB.COM",
                LockoutEnabled = true,
                SecurityStamp = "JK82L91TB50IX67PO92302JSD30NGF",
                IsApproved = true
            };

            adminUser.PasswordHash = hasher.HashPassword(adminUser, "admin");

            User author = new User
            {
                Id = 2,
                UserName = "author@insighthub.com",
                FirstName = "John",
                LastName = "Doe",
                NormalizedUserName = "AUTHOR@INSIGHTHUB.COM",
                Email = "author@insighthub.com",
                NormalizedEmail = "AUTHOR@INSIGHTHUB.COM",
                LockoutEnabled = true,
                SecurityStamp = "K3NV7340BJD72PJCDHSI37GZR409JL",
                IsApproved = true
            };

            author.PasswordHash = hasher.HashPassword(author, "author");

            User client = new User
            {
                Id = 3,
                UserName = "client@insighthub.com",
                FirstName = "Jane",
                LastName = "Doe",
                NormalizedUserName = "CLIENT@INSIGHTHUB.COM",
                Email = "client@insighthub.com",
                NormalizedEmail = "CLIENT@INSIGHTHUB.COM",
                LockoutEnabled = true,
                SecurityStamp = "SK83NVJDI39TDOW028FJSDKOAO280",
            };

            client.PasswordHash = hasher.HashPassword(client, "client");

            User secondAuthor = new User
            {
                Id = 4,
                UserName = "secondAuthor@insighthub.com",
                FirstName = "Jack",
                LastName = "Doe",
                NormalizedUserName = "SECONDAUTHOR@INSIGHTHUB.COM",
                Email = "secondAuthor@insighthub.com",
                NormalizedEmail = "SECONDAUTHOR@INSIGHTHUB.COM",
                LockoutEnabled = true,
                SecurityStamp = "KFH2858JDHKG015NV234KJHDN6583",
                IsApproved = true
            };

            secondAuthor.PasswordHash = hasher.HashPassword(secondAuthor, "author");

            builder.Entity<User>().HasData(adminUser, client, author, secondAuthor);

            builder.Entity<IdentityUserRole<int>>().HasData(
                new IdentityUserRole<int>
                {
                    RoleId = 1,
                    UserId = adminUser.Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = author.Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 3,
                    UserId = client.Id
                },
                new IdentityUserRole<int>
                {
                    RoleId = 2,
                    UserId = secondAuthor.Id
                });

            var financialServices = new Industry
            {
                Id = 1,
                Name = "Financial Services",
            };
            var retail = new Industry
            {
                Id = 2,
                Name = "Retail",
            };
            var healthcare = new Industry
            {
                Id = 3,
                Name = "Healthcare",
            };
            var manifacturing = new Industry
            {
                Id = 4,
                Name = "Manufacturing",
            };
            var publicSector = new Industry
            {
                Id = 5,
                Name = "Public Sector",
            };
            var consumerElectronics = new Industry
            {
                Id = 6,
                Name = "Consumer Electronics",
            };
            var mediaAndEntertainment = new Industry
            {
                Id = 7,
                Name = "Media & Entertainment",
            };
            var travel = new Industry
            {
                Id = 8,
                Name = "Travel",
            };
            var transportationAndLogistics = new Industry
            {
                Id = 9,
                Name = "Transportation & Logistics",
            };
            var energyAndUtilities = new Industry
            {
                Id = 10,
                Name = "Energy & Utilities",
            };
            var other = new Industry
            {
                Id = 11,
                Name = "Other",
            };

            builder.Entity<Industry>().HasData(financialServices, retail, healthcare, manifacturing, publicSector,
                consumerElectronics, mediaAndEntertainment, travel, transportationAndLogistics, energyAndUtilities, other);


            var businessAndITAlignment = new Tag
            {
                Id = 1,
                Name = "Business & IT Alignment"
            };
            var techSectorEconomics = new Tag
            {
                Id = 2,
                Name = "Tech Sector Economics"
            };
            var strategyPlanningAndGovernance = new Tag
            {
                Id = 3,
                Name = "Strategy Planning & Governance"
            };
            var informationSecurity = new Tag
            {
                Id = 4,
                Name = "Information Security"
            };
            var digitalMarketing = new Tag
            {
                Id = 5,
                Name = "Digital Marketing"
            };
            var iTServices = new Tag
            {
                Id = 6,
                Name = "IT Services"
            };

            builder.Entity<Tag>().HasData(businessAndITAlignment, techSectorEconomics, strategyPlanningAndGovernance,
                informationSecurity, digitalMarketing, iTServices);


            var techReport = new Report
            {
                Id = 1,
                Name = "Detail Your Three IoT Scenarios",
                Description = "Segmenting The Application Of Internet Of Things Across Products, Assets, And Ecosystem Sources",
                AuthorID = 2,
                IndustryID = 6,
                Content = "Detail Your Three IoT Scenarios.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(1),
                TimesDownloaded = 13,
                IsApproved = true
            };
            var financialReport = new Report
            {
                Id = 2,
                Name = "2020 To 2021 US Tech Budgets",
                Description = "CIOs Should Tighten Their 2020 To 2021 Tech Budgets Based On " +
                "Slowing But Still Positive US Economic Growth",
                AuthorID = 2,
                IndustryID = 1,
                Content = "2020 To 2021 US Tech Budgets.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(2),
                TimesDownloaded = 5,
                IsApproved = true
            };
            var medicalReport = new Report
            {
                Id = 3,
                Name = "New Tech: Medical Device Security",
                Description = "Forrester's Landscape Overview Of 19 Providers",
                AuthorID = 4,
                IndustryID = 3,
                Content = "New Tech Medical Device Security.pdf",
                CreatedOn = DateTime.Now,
                TimesDownloaded = 8,
                IsApproved = true
            };
            var financialReport2 = new Report
            {
                Id = 4,
                Name = "New Tech: Open Banking Intermediaries, Q2 2020",
                Description = "Forrester's Landscape Overview Of 35 Providers",
                AuthorID = 2,
                IndustryID = 1,
                Content = "New Tech Open Banking Intermediaries, Q2 2020.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(1),
                TimesDownloaded = 17,
                IsApproved = true,
                IsFeatured = true
            };
            var retailReport = new Report
            {
                Id = 5,
                Name = "Your Strategic Plan Is The Linchpin For Omnichannel Success",
                Description = "Strategic Plan: The Omnichannel Commerce Playbook",
                AuthorID = 4,
                IndustryID = 2,
                Content = "Your Strategic Plan Is The Linchpin For Omnichannel Success.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(2),
                TimesDownloaded = 2,
                IsApproved = true,
                IsFeatured = true
            };
            var publicSectorReport = new Report
            {
                Id = 6,
                Name = "India Tech Market Outlook, 2020 To 2021",
                Description = "COVID-19 Has All But Killed India's Tech Spending Growth",
                AuthorID = 2,
                IndustryID = 5,
                Content = "India Tech Market Outlook, 2020 To 2021.pdf",
                CreatedOn = DateTime.Now,
                TimesDownloaded = 2,
                IsApproved = true,
                IsFeatured = true
            };
            var transportLogisticsReport = new Report
            {
                Id = 7,
                Name = "Now Tech: Sales Enablement Automation, Q2 2020",
                Description = "Forrester's Overview Of 32 Sales Enablement Automation Providers",
                AuthorID = 4,
                IndustryID = 9,
                Content = "Now Tech Sales Enablement Automation, Q2 2020.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(1),
                TimesDownloaded = 13,
                IsApproved = true,
                IsFeatured = true
            };
            var transportLogisticsReport2 = new Report
            {
                Id = 8,
                Name = "Select The Right IoT Monitoring Strategy",
                Description = "Tackle The Key IoT Challenges That Stress Existing Enterprise Monitoring",
                AuthorID = 4,
                IndustryID = 9,
                Content = "Select The Right IoT Monitoring Strategy.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(2),
                TimesDownloaded = 5,
                IsApproved = true,
                IsFeatured = true
            };
            var consumerElectronicsReport = new Report
            {
                Id = 9,
                Name = "Ten Golden Rules For RPA Success",
                Description = "Focus On These Key Philosophies For Lasting Automation Value",
                AuthorID = 2,
                IndustryID = 6,
                Content = "Ten Golden Rules For RPA Success.pdf",
                CreatedOn = DateTime.Now,
                TimesDownloaded = 6,
                IsApproved = true
            };
            var consumerElectronicsReport2 = new Report
            {
                Id = 10,
                Name = "How To Evolve Your Mobile App",
                Description = "Make Smart Investment Choices Based On Your Customer's Goals And Needs",
                AuthorID = 4,
                IndustryID = 6,
                Content = "How To Evolve Your Mobile App.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(1),
                TimesDownloaded = 34,
                IsApproved = true
            };
            var mediaEntertainmentReport = new Report
            {
                Id = 11,
                Name = "Enterprise Architecture In 2025 And Beyond",
                Description = "Vision: The EA Practice Playbook",
                AuthorID = 2,
                IndustryID = 7,
                Content = "Enterprise Architecture In 2025 And Beyond.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(2),
                TimesDownloaded = 2,
                IsApproved = true,
            };
            var energyReport = new Report
            {
                Id = 12,
                Name = "PEAK Leadership Powers Great EX",
                Description = "Engage The Adaptive Workforce With EX-Centered Leadership",
                AuthorID = 4,
                IndustryID = 10,
                Content = "PEAK Leadership Powers Great EX.pdf",
                CreatedOn = DateTime.Now,
                TimesDownloaded = 0,
                IsApproved = true,
            };
            var travelReport = new Report
            {
                Id = 13,
                Name = "Create A Governance Strategy To Meet The Process Imperative",
                Description = "The Shift Required To Drive Process Automation At Scale",
                AuthorID = 2,
                IndustryID = 8,
                Content = "Create A Governance Strategy To Meet The Process Imperative.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(1),
                TimesDownloaded = 1,
                IsApproved = true,
            };
            var mediaEntertainmentReport2 = new Report
            {
                Id = 14,
                Name = "How To Evaluate Marketing Technology Performance",
                Description = "Performance Management: The Enterprise Marketing Technology Playbook",
                AuthorID = 4,
                IndustryID = 7,
                Content = "How To Evaluate Marketing Technology Performance.pdf",
                CreatedOn = DateTime.UtcNow.AddDays(2),
                TimesDownloaded = 5,
                IsApproved = true,
            };
            var mediaEntertainmentReport3 = new Report
            {
                Id = 15,
                Name = "Research Overview: Artificial Intelligence",
                Description = "A Guide To Navigating Our AI Technology Research Portfolio",
                AuthorID = 2,
                IndustryID = 7,
                Content = "Research Overview Artificial Intelligence.pdf",
                CreatedOn = DateTime.Now,
                TimesDownloaded = 23,
                IsApproved = true,
            };

            builder.Entity<Report>().HasData(techReport, financialReport, medicalReport, retailReport, financialReport2, 
                publicSectorReport, transportLogisticsReport, transportLogisticsReport2, mediaEntertainmentReport3, mediaEntertainmentReport2,
                mediaEntertainmentReport, travelReport, energyReport, consumerElectronicsReport, consumerElectronicsReport2);


            var reportTag1 = new ReportTag
            {
                ReportId = techReport.Id,
                TagId = techSectorEconomics.Id
            };
            var reportTag2 = new ReportTag
            {
                ReportId = techReport.Id,
                TagId = iTServices.Id
            };
            var reportTag3 = new ReportTag
            {
                ReportId = financialReport.Id,
                TagId = businessAndITAlignment.Id
            };
            var reportTag4 = new ReportTag
            {
                ReportId = financialReport.Id,
                TagId = techSectorEconomics.Id
            };
            var reportTag5 = new ReportTag
            {
                ReportId = financialReport.Id,
                TagId = strategyPlanningAndGovernance.Id
            };
            var reportTag6 = new ReportTag
            {
                ReportId = medicalReport.Id,
                TagId = strategyPlanningAndGovernance.Id
            };
            var reportTag7 = new ReportTag
            {
                ReportId = medicalReport.Id,
                TagId = iTServices.Id
            };
            var reportTag8 = new ReportTag
            {
                ReportId = 6,
                TagId = 6
            };
            var reportTag9 = new ReportTag
            {
                ReportId = 6,
                TagId = 2
            };
            var reportTag10 = new ReportTag
            {
                ReportId = 7,
                TagId = 1
            };
            var reportTag11 = new ReportTag
            {
                ReportId = 7,
                TagId = 2
            };
            var reportTag12 = new ReportTag
            {
                ReportId = 8,
                TagId = 3
            };
            var reportTag13 = new ReportTag
            {
                ReportId = 9,
                TagId = 4
            };
            var reportTag14 = new ReportTag
            {
                ReportId = 10,
                TagId = 3
            };
            var reportTag15 = new ReportTag
            {
                ReportId = 11,
                TagId = 5
            };
            var reportTag16 = new ReportTag
            {
                ReportId = 12,
                TagId = 6
            };
            var reportTag17 = new ReportTag
            {
                ReportId = 13,
                TagId = 1
            };
            var reportTag18 = new ReportTag
            {
                ReportId = 14,
                TagId = 4
            };
            var reportTag19 = new ReportTag
            {
                ReportId = 15,
                TagId = 5
            };
            var reportTag20 = new ReportTag
            {
                ReportId = 5,
                TagId = 6
            };
            var reportTag21 = new ReportTag
            {
                ReportId = 4,
                TagId = 5
            };

            builder.Entity<ReportTag>().HasData(reportTag1, reportTag2, reportTag3, reportTag4, reportTag5, reportTag6, reportTag7
                , reportTag8, reportTag9, reportTag10, reportTag11, reportTag12, reportTag13, reportTag14, reportTag15, reportTag16, reportTag17
                , reportTag18, reportTag19, reportTag20, reportTag21);

            var subscription1 = new UserIndustry
            {
                UserId = client.Id,
                IndustryId = publicSector.Id
            };
            var subscription2 = new UserIndustry
            {
                UserId = client.Id,
                IndustryId = mediaAndEntertainment.Id
            };
            var subscription3 = new UserIndustry
            {
                UserId = client.Id,
                IndustryId = travel.Id
            };

            builder.Entity<UserIndustry>().HasData(subscription1, subscription2, subscription3);

            var downloader = new UserReport
            {
                UserId = author.Id,
                ReportId = techReport.Id
            };
            var downloader2 = new UserReport
            {
                UserId = author.Id,
                ReportId = financialReport.Id
            };
            var downloader3 = new UserReport
            {
                UserId = author.Id,
                ReportId = medicalReport.Id
            };

            builder.Entity<UserReport>().HasData(downloader, downloader2, downloader3);
        }
    }
}
