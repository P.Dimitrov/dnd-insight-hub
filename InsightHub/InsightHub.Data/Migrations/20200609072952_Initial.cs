﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InsightHub.Data.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ChangedOn = table.Column<DateTime>(nullable: true),
                    ApprovedOn = table.Column<DateTime>(nullable: true),
                    IsApproved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Industries",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 40, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Industries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tags",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tags", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Reports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: false),
                    IndustryID = table.Column<int>(nullable: false),
                    Content = table.Column<string>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    ChangedOn = table.Column<DateTime>(nullable: true),
                    IsApproved = table.Column<bool>(nullable: false),
                    ApprovedOn = table.Column<DateTime>(nullable: true),
                    IsFeatured = table.Column<bool>(nullable: false),
                    AuthorID = table.Column<int>(nullable: false),
                    TimesDownloaded = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reports_AspNetUsers_AuthorID",
                        column: x => x.AuthorID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reports_Industries_IndustryID",
                        column: x => x.IndustryID,
                        principalTable: "Industries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserIndustries",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    IndustryId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserIndustries", x => new { x.UserId, x.IndustryId });
                    table.ForeignKey(
                        name: "FK_UserIndustries_Industries_IndustryId",
                        column: x => x.IndustryId,
                        principalTable: "Industries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserIndustries_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReportTags",
                columns: table => new
                {
                    ReportId = table.Column<int>(nullable: false),
                    TagId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportTags", x => new { x.ReportId, x.TagId });
                    table.ForeignKey(
                        name: "FK_ReportTags_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReportTags_Tags_TagId",
                        column: x => x.TagId,
                        principalTable: "Tags",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserReports",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    ReportId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserReports", x => new { x.UserId, x.ReportId });
                    table.ForeignKey(
                        name: "FK_UserReports_Reports_ReportId",
                        column: x => x.ReportId,
                        principalTable: "Reports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserReports_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "5728b7d2-0d5a-49e2-8932-cd861b35abac", "Admin", "ADMIN" },
                    { 3, "187aee24-0dbe-40b3-859b-d58668c03a51", "Client", "Client" },
                    { 2, "701f12a3-e96d-4fc2-8fec-11b632a731a1", "Author", "Author" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ApprovedOn", "ChangedOn", "ConcurrencyStamp", "CreatedOn", "Email", "EmailConfirmed", "FirstName", "IsApproved", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 3, 0, null, null, "0f76df76-95fe-41f8-8910-867943081f67", new DateTime(2020, 6, 9, 7, 29, 52, 138, DateTimeKind.Utc).AddTicks(1191), "client@insighthub.com", false, "Jane", false, "Doe", true, null, "CLIENT@INSIGHTHUB.COM", "CLIENT@INSIGHTHUB.COM", "AQAAAAEAACcQAAAAEN+v8/ABpDFBncp+7++LAy9dhbeswFKmHo7ZuC6smN2BZiF1CoM4piGTYnJ/3XJnGw==", null, false, "SK83NVJDI39TDOW028FJSDKOAO280", false, "client@insighthub.com" },
                    { 1, 0, null, null, "0b8b367c-6b16-4168-8d2d-d4c90b2e046f", new DateTime(2020, 6, 9, 7, 29, 52, 119, DateTimeKind.Utc).AddTicks(9993), "admin@insighthub.com", false, "Admin", true, "Admin", true, null, "ADMIN@INSIGHTHUB.COM", "ADMIN@INSIGHTHUB.COM", "AQAAAAEAACcQAAAAEPUW2Xy5UaSupgK0n5rON+XPWSyK7NmFnJfrUjMhDXnutw5HIgaZJfzBllYDkiaZgQ==", null, false, "JK82L91TB50IX67PO92302JSD30NGF", false, "admin@insighthub.com" },
                    { 2, 0, null, null, "b4e51a6c-67ec-47a9-9668-f67ffface2c0", new DateTime(2020, 6, 9, 7, 29, 52, 130, DateTimeKind.Utc).AddTicks(8556), "author@insighthub.com", false, "John", true, "Doe", true, null, "AUTHOR@INSIGHTHUB.COM", "AUTHOR@INSIGHTHUB.COM", "AQAAAAEAACcQAAAAEMmSChAAL1fG2iu/UKgvvUNdBcbzIAsSwB4yl0U6UntbakPIuqFqlzL/dftMizmFLA==", null, false, "K3NV7340BJD72PJCDHSI37GZR409JL", false, "author@insighthub.com" },
                    { 4, 0, null, null, "6c627f4b-4f64-4c7e-aa8b-b3d034526d64", new DateTime(2020, 6, 9, 7, 29, 52, 145, DateTimeKind.Utc).AddTicks(7270), "secondAuthor@insighthub.com", false, "Jack", true, "Doe", true, null, "SECONDAUTHOR@INSIGHTHUB.COM", "SECONDAUTHOR@INSIGHTHUB.COM", "AQAAAAEAACcQAAAAEC3CVH7sN+/5WbZDO+au1aNG7jblWpf6XhWbOZHRYpC3NxPNrV54MC61z64RbDXGvA==", null, false, "KFH2858JDHKG015NV234KJHDN6583", false, "secondAuthor@insighthub.com" }
                });

            migrationBuilder.InsertData(
                table: "Industries",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 8, "Travel" },
                    { 2, "Retail" },
                    { 3, "Healthcare" },
                    { 7, "Media & Entertainment" },
                    { 4, "Manufacturing" },
                    { 1, "Financial Services" },
                    { 6, "Consumer Electronics" },
                    { 11, "Other" },
                    { 10, "Energy & Utilities" },
                    { 9, "Transportation & Logistics" },
                    { 5, "Public Sector" }
                });

            migrationBuilder.InsertData(
                table: "Tags",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Business & IT Alignment" },
                    { 3, "Strategy Planning & Governance" },
                    { 4, "Information Security" },
                    { 5, "Digital Marketing" },
                    { 6, "IT Services" },
                    { 2, "Tech Sector Economics" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 2 }
                });

            migrationBuilder.InsertData(
                table: "Reports",
                columns: new[] { "Id", "ApprovedOn", "AuthorID", "ChangedOn", "Content", "CreatedOn", "Description", "IndustryID", "IsApproved", "IsFeatured", "Name", "TimesDownloaded" },
                values: new object[,]
                {
                    { 9, null, 2, null, "Ten Golden Rules For RPA Success.pdf", new DateTime(2020, 6, 9, 10, 29, 52, 156, DateTimeKind.Local).AddTicks(8342), "Focus On These Key Philosophies For Lasting Automation Value", 6, true, false, "Ten Golden Rules For RPA Success", 6 },
                    { 12, null, 4, null, "PEAK Leadership Powers Great EX.pdf", new DateTime(2020, 6, 9, 10, 29, 52, 156, DateTimeKind.Local).AddTicks(8354), "Engage The Adaptive Workforce With EX-Centered Leadership", 10, true, false, "PEAK Leadership Powers Great EX", 0 },
                    { 14, null, 4, null, "How To Evaluate Marketing Technology Performance.pdf", new DateTime(2020, 6, 11, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(8362), "Performance Management: The Enterprise Marketing Technology Playbook", 7, true, false, "How To Evaluate Marketing Technology Performance", 5 },
                    { 8, null, 4, null, "Select The Right IoT Monitoring Strategy.pdf", new DateTime(2020, 6, 11, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(8339), "Tackle The Key IoT Challenges That Stress Existing Enterprise Monitoring", 9, true, true, "Select The Right IoT Monitoring Strategy", 5 },
                    { 7, null, 4, null, "Now Tech Sales Enablement Automation, Q2 2020.pdf", new DateTime(2020, 6, 10, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(8336), "Forrester's Overview Of 32 Sales Enablement Automation Providers", 9, true, true, "Now Tech: Sales Enablement Automation, Q2 2020", 13 },
                    { 5, null, 4, null, "Your Strategic Plan Is The Linchpin For Omnichannel Success.pdf", new DateTime(2020, 6, 11, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(8283), "Strategic Plan: The Omnichannel Commerce Playbook", 2, true, true, "Your Strategic Plan Is The Linchpin For Omnichannel Success", 2 },
                    { 3, null, 4, null, "New Tech Medical Device Security.pdf", new DateTime(2020, 6, 9, 10, 29, 52, 154, DateTimeKind.Local).AddTicks(2256), "Forrester's Landscape Overview Of 19 Providers", 3, true, false, "New Tech: Medical Device Security", 8 },
                    { 11, null, 2, null, "Enterprise Architecture In 2025 And Beyond.pdf", new DateTime(2020, 6, 11, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(8351), "Vision: The EA Practice Playbook", 7, true, false, "Enterprise Architecture In 2025 And Beyond", 2 },
                    { 10, null, 4, null, "How To Evolve Your Mobile App.pdf", new DateTime(2020, 6, 10, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(8349), "Make Smart Investment Choices Based On Your Customer's Goals And Needs", 6, true, false, "How To Evolve Your Mobile App", 34 },
                    { 15, null, 2, null, "Research Overview Artificial Intelligence.pdf", new DateTime(2020, 6, 9, 10, 29, 52, 156, DateTimeKind.Local).AddTicks(8366), "A Guide To Navigating Our AI Technology Research Portfolio", 7, true, false, "Research Overview: Artificial Intelligence", 23 },
                    { 6, null, 2, null, "India Tech Market Outlook, 2020 To 2021.pdf", new DateTime(2020, 6, 9, 10, 29, 52, 156, DateTimeKind.Local).AddTicks(8305), "COVID-19 Has All But Killed India's Tech Spending Growth", 5, true, true, "India Tech Market Outlook, 2020 To 2021", 2 },
                    { 4, null, 2, null, "New Tech Open Banking Intermediaries, Q2 2020.pdf", new DateTime(2020, 6, 10, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(7505), "Forrester's Landscape Overview Of 35 Providers", 1, true, true, "New Tech: Open Banking Intermediaries, Q2 2020", 17 },
                    { 2, null, 2, null, "2020 To 2021 US Tech Budgets.pdf", new DateTime(2020, 6, 11, 7, 29, 52, 154, DateTimeKind.Utc).AddTicks(2190), "CIOs Should Tighten Their 2020 To 2021 Tech Budgets Based On Slowing But Still Positive US Economic Growth", 1, true, false, "2020 To 2021 US Tech Budgets", 5 },
                    { 1, null, 2, null, "Detail Your Three IoT Scenarios.pdf", new DateTime(2020, 6, 10, 7, 29, 52, 154, DateTimeKind.Utc).AddTicks(595), "Segmenting The Application Of Internet Of Things Across Products, Assets, And Ecosystem Sources", 6, true, false, "Detail Your Three IoT Scenarios", 13 },
                    { 13, null, 2, null, "Create A Governance Strategy To Meet The Process Imperative.pdf", new DateTime(2020, 6, 10, 7, 29, 52, 156, DateTimeKind.Utc).AddTicks(8360), "The Shift Required To Drive Process Automation At Scale", 8, true, false, "Create A Governance Strategy To Meet The Process Imperative", 1 }
                });

            migrationBuilder.InsertData(
                table: "UserIndustries",
                columns: new[] { "UserId", "IndustryId" },
                values: new object[,]
                {
                    { 3, 8 },
                    { 3, 5 },
                    { 3, 7 }
                });

            migrationBuilder.InsertData(
                table: "ReportTags",
                columns: new[] { "ReportId", "TagId" },
                values: new object[,]
                {
                    { 1, 2 },
                    { 14, 4 },
                    { 8, 3 },
                    { 7, 2 },
                    { 7, 1 },
                    { 5, 6 },
                    { 3, 6 },
                    { 3, 3 },
                    { 9, 4 },
                    { 13, 1 },
                    { 12, 6 },
                    { 11, 5 },
                    { 6, 2 },
                    { 6, 6 },
                    { 4, 5 },
                    { 2, 3 },
                    { 2, 2 },
                    { 2, 1 },
                    { 1, 6 },
                    { 15, 5 },
                    { 10, 3 }
                });

            migrationBuilder.InsertData(
                table: "UserReports",
                columns: new[] { "UserId", "ReportId" },
                values: new object[,]
                {
                    { 2, 2 },
                    { 2, 3 },
                    { 2, 1 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AuthorID",
                table: "Reports",
                column: "AuthorID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_IndustryID",
                table: "Reports",
                column: "IndustryID");

            migrationBuilder.CreateIndex(
                name: "IX_ReportTags_TagId",
                table: "ReportTags",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_UserIndustries_IndustryId",
                table: "UserIndustries",
                column: "IndustryId");

            migrationBuilder.CreateIndex(
                name: "IX_UserReports_ReportId",
                table: "UserReports",
                column: "ReportId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "ReportTags");

            migrationBuilder.DropTable(
                name: "UserIndustries");

            migrationBuilder.DropTable(
                name: "UserReports");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Tags");

            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Industries");
        }
    }
}
