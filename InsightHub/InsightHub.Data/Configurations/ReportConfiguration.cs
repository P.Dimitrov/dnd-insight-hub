﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of Report entity.
    /// </summary>
    public class ReportConfiguration : IEntityTypeConfiguration<Report>
    {
        public void Configure(EntityTypeBuilder<Report> builder)
        {
            builder.HasKey(r => r.Id);

            builder.Property(r => r.Name)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(r => r.Description)
                .HasMaxLength(500)
                .IsRequired();

            builder.HasOne(r => r.Industry)
                .WithMany(i => i.Reports)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasMany(r => r.ReportTags)
                .WithOne(rt => rt.Report)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Property(r => r.Id)
                .IsRequired();

            builder.HasOne(r => r.Author)
                .WithMany(a => a.AuthoredReports)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.Property(r => r.Content)
                .IsRequired();

            builder.Property(r => r.CreatedOn)
                .IsRequired();

            builder.Property(r => r.ChangedOn);

            builder.Property(r => r.ApprovedOn);

            builder.Property(r => r.IsApproved)
                .IsRequired();

            builder.Property(r => r.IsFeatured)
                .IsRequired();

            builder.Property(r => r.TimesDownloaded)
                .IsRequired();
        }
    }
}
