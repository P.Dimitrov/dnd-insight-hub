﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of ReportTag entity.
    /// </summary>
    public class ReportTagConfiguration : IEntityTypeConfiguration<ReportTag>
    {
        public void Configure(EntityTypeBuilder<ReportTag> builder)
        {
            builder.HasKey(rt => new
            {
                rt.ReportId,
                rt.TagId
            });

            builder.HasOne(r => r.Report)
                .WithMany(rt => rt.ReportTags)
                .HasForeignKey(k => k.ReportId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(t => t.Tag)
                .WithMany(rt => rt.ReportTags)
                .HasForeignKey(k => k.TagId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }

    }
}
