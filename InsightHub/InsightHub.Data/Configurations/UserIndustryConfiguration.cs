﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of UserIndustry entity.
    /// </summary>
    public class UserIndustryConfiguration : IEntityTypeConfiguration<UserIndustry>
    {
        public void Configure(EntityTypeBuilder<UserIndustry> builder)
        {
            builder.HasKey(ui => new
            {
                ui.UserId,
                ui.IndustryId
            });

            builder.HasOne(ui => ui.User)
                .WithMany(u => u.Subscriptions)
                .HasForeignKey(ui => ui.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(ui => ui.Industry)
                .WithMany(i => i.Subscribers)
                .HasForeignKey(ui => ui.IndustryId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
