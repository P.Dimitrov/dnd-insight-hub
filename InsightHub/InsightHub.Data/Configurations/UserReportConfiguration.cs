﻿using InsightHub.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace InsightHub.Data.Configurations
{
    /// <summary>
    /// A class that implements the configuration properties of UserReport entity.
    /// </summary>
    public class UserReportConfiguration : IEntityTypeConfiguration<UserReport>
    {
        public void Configure(EntityTypeBuilder<UserReport> builder)
        {
            builder.HasKey(ur => new
            {
                ur.UserId,
                ur.ReportId
            });

            builder.HasOne(ur => ur.User)
                .WithMany(u => u.DownloadedReports)
                .HasForeignKey(ui => ui.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder.HasOne(ur => ur.Report)
                .WithMany(r => r.Customers)
                .HasForeignKey(ui => ui.ReportId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
