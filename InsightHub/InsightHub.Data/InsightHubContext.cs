﻿using InsightHub.Data.Configurations;
using InsightHub.Data.Seeder;
using InsightHub.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace InsightHub.Data
{
    /// <summary>
    /// A class that sets the Database columns via DbSets, injects 
    /// the model configurations in the OnModel method and initializes the Seeder..
    /// </summary>
    public class InsightHubContext : IdentityDbContext<User, Role, int>
    {
        public InsightHubContext(DbContextOptions<InsightHubContext> options)
            : base(options)
        {

        }

        public DbSet<Industry> Industries { get; set; }

        public DbSet<Report> Reports { get; set; }

        public DbSet<Tag> Tags { get; set; }

        public DbSet<ReportTag> ReportTags { get; set; }

        public DbSet<UserIndustry> UserIndustries { get; set; }

        public DbSet<UserReport> UserReports { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new IndustryConfiguration());
            builder.ApplyConfiguration(new ReportConfiguration());
            builder.ApplyConfiguration(new ReportTagConfiguration());
            builder.ApplyConfiguration(new TagConfiguration());
            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new UserIndustryConfiguration());
            builder.ApplyConfiguration(new UserReportConfiguration());

            builder.InitialSeed();

            base.OnModelCreating(builder);
        }
    }
}
