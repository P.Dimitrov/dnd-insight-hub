## Insight Hub
Web application for delivering access to reports for major business and technology trends.

<br/>
![HomePage](Images/HomePage.png)

## Areas
- Public - accessible without authentication
- Private - available after registration
- Administrative - available for admins only
---
## Sections
- [Public part](#Public-part)
- [Private part](#Private-part)
- [Administrative part](#Administrative-part)

# Public part
 - Check out the newest, most downloaded and featured reports.

## Report browser
![Reports page](Images/all_reports.png)

 - Browse reports by name, description, industry, tags, authors and date of creation.
 - Direct link to report's details

# Private part

## Client
### Options
- Browse current reports by different criteria
- Download PDF files

![Client page](Images/download_report.png)

<br>

## Author
### Options
- Create reports with ability to add new Tags dynamicly
- Modify current reports
- Upload PDF file

![Create report](Images/create_report.png)

---

# Administrative part

 - Administrators have full control over user and reports approval. Also the Admin can create, edit and delete Industries and Tags.

![Pending reports](Images/admin_control.png)


## Technologies

* ASP.NET Core
* ASP.NET Identity
* Entity Framework Core
* MS SQL Server
* Razor
* AJAX
* JavaScript / jQuery
* HTML
* CSS
* Bootstrap

## Additional information

* Unit tests covering 95% of the business logic
* Used JWT for API authorization
* Used Azure Blob Storage to handle the binary content
* Used Gitflow Workflow throughout the development process
* Implemented Continuous Integration with Gitlab
* Followed the Kanban methodology regarding task management

## API Documentation

* [Swagger](https://localhost:44333/swagger/index.html)

## Team Members

* Iva Dimitrova - [GitLab](https://gitlab.com/Alhana)
* Petar Dimitrov - [GitLab](https://gitlab.com/P.Dimitrov)

## Credits
* Radko Stanev
* Anton Gervaziev

